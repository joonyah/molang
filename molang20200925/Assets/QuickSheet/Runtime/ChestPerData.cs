using UnityEngine;
using System.Collections;

///
/// !!! Machine generated code !!!
/// !!! DO NOT CHANGE Tabs to Spaces !!!
///
[System.Serializable]
public class ChestPerData
{
  [SerializeField]
  int stagelevel;
  public int Stagelevel { get {return stagelevel; } set { stagelevel = value;} }
  
  [SerializeField]
  string[] chest = new string[0];
  public string[] Chest { get {return chest; } set { chest = value;} }
  
  [SerializeField]
  float[] chestprobability = new float[0];
  public float[] Chestprobability { get {return chestprobability; } set { chestprobability = value;} }
  
  [SerializeField]
  float[] difficultyeasyx = new float[0];
  public float[] Difficultyeasyx { get {return difficultyeasyx; } set { difficultyeasyx = value;} }
  
  [SerializeField]
  float[] difficultynormalx = new float[0];
  public float[] Difficultynormalx { get {return difficultynormalx; } set { difficultynormalx = value;} }
  
  [SerializeField]
  float[] difficultyhard = new float[0];
  public float[] Difficultyhard { get {return difficultyhard; } set { difficultyhard = value;} }
  
  [SerializeField]
  float[] difficultyhellx = new float[0];
  public float[] Difficultyhellx { get {return difficultyhellx; } set { difficultyhellx = value;} }
  
}