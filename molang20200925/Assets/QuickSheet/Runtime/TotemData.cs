using UnityEngine;
using System.Collections;

///
/// !!! Machine generated code !!!
/// !!! DO NOT CHANGE Tabs to Spaces !!!
///
[System.Serializable]
public class TotemData
{
  [SerializeField]
  string id;
  public string ID { get {return id; } set { id = value;} }
  
  [SerializeField]
  string actiontype;
  public string Actiontype { get {return actiontype; } set { actiontype = value;} }
  
  [SerializeField]
  string[] diminishtype = new string[0];
  public string[] Diminishtype { get {return diminishtype; } set { diminishtype = value;} }
  
  [SerializeField]
  int[] diminishamount = new int[0];
  public int[] Diminishamount { get {return diminishamount; } set { diminishamount = value;} }
  
  [SerializeField]
  string[] increasetype = new string[0];
  public string[] Increasetype { get {return increasetype; } set { increasetype = value;} }
  
  [SerializeField]
  int[] increaseamount = new int[0];
  public int[] Increaseamount { get {return increaseamount; } set { increaseamount = value;} }
  
}