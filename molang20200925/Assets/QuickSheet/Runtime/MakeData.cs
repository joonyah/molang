using UnityEngine;
using System.Collections;

///
/// !!! Machine generated code !!!
/// !!! DO NOT CHANGE Tabs to Spaces !!!
///
[System.Serializable]
public class MakeData
{
  [SerializeField]
  string id;
  public string ID { get {return id; } set { id = value;} }
  
  [SerializeField]
  string name;
  public string NAME { get {return name; } set { name = value;} }
  
  [SerializeField]
  string type;
  public string TYPE { get {return type; } set { type = value;} }
  
  [SerializeField]
  int amount;
  public int AMOUNT { get {return amount; } set { amount = value;} }
  
  [SerializeField]
  int price;
  public int PRICE { get {return price; } set { price = value;} }
  
  [SerializeField]
  bool islock;
  public bool ISLOCK { get {return islock; } set { islock = value;} }
  
}