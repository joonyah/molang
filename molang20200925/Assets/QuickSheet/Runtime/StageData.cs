using UnityEngine;
using System.Collections;

///
/// !!! Machine generated code !!!
/// !!! DO NOT CHANGE Tabs to Spaces !!!
///
[System.Serializable]
public class StageData
{
  [SerializeField]
  int seq;
  public int SEQ { get {return seq; } set { seq = value;} }
  
  [SerializeField]
  string type1;
  public string Type1 { get {return type1; } set { type1 = value;} }
  
  [SerializeField]
  string type2;
  public string Type2 { get {return type2; } set { type2 = value;} }
  
  [SerializeField]
  string[] roomid = new string[0];
  public string[] Roomid { get {return roomid; } set { roomid = value;} }
  
  [SerializeField]
  float percentage;
  public float Percentage { get {return percentage; } set { percentage = value;} }
  
  [SerializeField]
  int[] roomcount = new int[0];
  public int[] Roomcount { get {return roomcount; } set { roomcount = value;} }
  
  [SerializeField]
  int[] health = new int[0];
  public int[] Health { get {return health; } set { health = value;} }
  
  [SerializeField]
  int[] damage = new int[0];
  public int[] Damage { get {return damage; } set { damage = value;} }
  
  [SerializeField]
  string[] chest = new string[0];
  public string[] Chest { get {return chest; } set { chest = value;} }
  
  [SerializeField]
  float[] chestprobability = new float[0];
  public float[] Chestprobability { get {return chestprobability; } set { chestprobability = value;} }
  
}