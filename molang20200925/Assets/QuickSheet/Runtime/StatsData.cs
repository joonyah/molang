using UnityEngine;
using System.Collections;

///
/// !!! Machine generated code !!!
/// !!! DO NOT CHANGE Tabs to Spaces !!!
///
[System.Serializable]
public class StatsData
{
  [SerializeField]
  string id;
  public string ID { get {return id; } set { id = value;} }
  
  [SerializeField]
  string type;
  public string Type { get {return type; } set { type = value;} }
  
  [SerializeField]
  int steps;
  public int Steps { get {return steps; } set { steps = value;} }
  
  [SerializeField]
  string name;
  public string Name { get {return name; } set { name = value;} }
  
  [SerializeField]
  string cont;
  public string Cont { get {return cont; } set { cont = value;} }
  
  [SerializeField]
  string consume;
  public string Consume { get {return consume; } set { consume = value;} }
  
  [SerializeField]
  int count;
  public int Count { get {return count; } set { count = value;} }
  
  [SerializeField]
  bool sum;
  public bool Sum { get {return sum; } set { sum = value;} }
  
}