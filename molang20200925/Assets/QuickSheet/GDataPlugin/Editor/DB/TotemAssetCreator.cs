using UnityEngine;
using UnityEditor;
using System.IO;
using UnityQuickSheet;

///
/// !!! Machine generated code !!!
/// 
public partial class GoogleDataAssetUtility
{
    [MenuItem("Assets/Create/Google/Totem")]
    public static void CreateTotemAssetFile()
    {
        Totem asset = CustomAssetUtility.CreateAsset<Totem>();
        asset.SheetName = "PJ_2nd_DBlist";
        asset.WorksheetName = "Totem";
        EditorUtility.SetDirty(asset);        
    }
    
}