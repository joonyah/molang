using UnityEngine;
using UnityEditor;
using System.IO;
using UnityQuickSheet;

///
/// !!! Machine generated code !!!
/// 
public partial class GoogleDataAssetUtility
{
    [MenuItem("Assets/Create/Google/Reinforce")]
    public static void CreateReinforceAssetFile()
    {
        Reinforce asset = CustomAssetUtility.CreateAsset<Reinforce>();
        asset.SheetName = "PJ_2nd_DBlist";
        asset.WorksheetName = "Reinforce";
        EditorUtility.SetDirty(asset);        
    }
    
}