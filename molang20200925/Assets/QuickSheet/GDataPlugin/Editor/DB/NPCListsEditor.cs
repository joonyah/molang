using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using GDataDB;
using GDataDB.Linq;

using UnityQuickSheet;

///
/// !!! Machine generated code !!!
///
[CustomEditor(typeof(NPCLists))]
public class NPCListsEditor : BaseGoogleEditor<NPCLists>
{	    
    public override bool Load()
    {        
        NPCLists targetData = target as NPCLists;
        
        var client = new DatabaseClient("", "");
        string error = string.Empty;
        var db = client.GetDatabase(targetData.SheetName, ref error);	
        var table = db.GetTable<NPCListsData>(targetData.WorksheetName) ?? db.CreateTable<NPCListsData>(targetData.WorksheetName);
        
        List<NPCListsData> myDataList = new List<NPCListsData>();
        
        var all = table.FindAll();
        foreach(var elem in all)
        {
            NPCListsData data = new NPCListsData();
            
            data = Cloner.DeepCopy<NPCListsData>(elem.Element);
            myDataList.Add(data);
        }
                
        targetData.dataArray = myDataList.ToArray();
        
        EditorUtility.SetDirty(targetData);
        AssetDatabase.SaveAssets();
        
        return true;
    }
}
