using UnityEngine;
using UnityEditor;
using System.IO;
using UnityQuickSheet;

///
/// !!! Machine generated code !!!
/// 
public partial class GoogleDataAssetUtility
{
    [MenuItem("Assets/Create/Google/Molang")]
    public static void CreateMolangAssetFile()
    {
        Molang asset = CustomAssetUtility.CreateAsset<Molang>();
        asset.SheetName = "PJ_Molang_DBlist";
        asset.WorksheetName = "Molang";
        EditorUtility.SetDirty(asset);        
    }
    
}