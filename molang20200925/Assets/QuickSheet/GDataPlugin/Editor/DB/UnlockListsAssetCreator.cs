using UnityEngine;
using UnityEditor;
using System.IO;
using UnityQuickSheet;

///
/// !!! Machine generated code !!!
/// 
public partial class GoogleDataAssetUtility
{
    [MenuItem("Assets/Create/Google/UnlockLists")]
    public static void CreateUnlockListsAssetFile()
    {
        UnlockLists asset = CustomAssetUtility.CreateAsset<UnlockLists>();
        asset.SheetName = "PJ_2nd_DBlist";
        asset.WorksheetName = "UnlockLists";
        EditorUtility.SetDirty(asset);        
    }
    
}