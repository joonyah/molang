using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using GDataDB;
using GDataDB.Linq;

using UnityQuickSheet;

///
/// !!! Machine generated code !!!
///
[CustomEditor(typeof(Stats))]
public class StatsEditor : BaseGoogleEditor<Stats>
{	    
    public override bool Load()
    {        
        Stats targetData = target as Stats;
        
        var client = new DatabaseClient("", "");
        string error = string.Empty;
        var db = client.GetDatabase(targetData.SheetName, ref error);	
        var table = db.GetTable<StatsData>(targetData.WorksheetName) ?? db.CreateTable<StatsData>(targetData.WorksheetName);
        
        List<StatsData> myDataList = new List<StatsData>();
        
        var all = table.FindAll();
        foreach(var elem in all)
        {
            StatsData data = new StatsData();
            
            data = Cloner.DeepCopy<StatsData>(elem.Element);
            myDataList.Add(data);
        }
                
        targetData.dataArray = myDataList.ToArray();
        
        EditorUtility.SetDirty(targetData);
        AssetDatabase.SaveAssets();
        
        return true;
    }
}
