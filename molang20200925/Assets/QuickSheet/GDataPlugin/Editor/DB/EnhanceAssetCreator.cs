using UnityEngine;
using UnityEditor;
using System.IO;
using UnityQuickSheet;

///
/// !!! Machine generated code !!!
/// 
public partial class GoogleDataAssetUtility
{
    [MenuItem("Assets/Create/Google/Enhance")]
    public static void CreateEnhanceAssetFile()
    {
        Enhance asset = CustomAssetUtility.CreateAsset<Enhance>();
        asset.SheetName = "PJ_2nd_DBlist";
        asset.WorksheetName = "Enhance";
        EditorUtility.SetDirty(asset);        
    }
    
}