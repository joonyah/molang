using UnityEngine;
using UnityEditor;
using System.IO;
using UnityQuickSheet;

///
/// !!! Machine generated code !!!
/// 
public partial class GoogleDataAssetUtility
{
    [MenuItem("Assets/Create/Google/Achievements")]
    public static void CreateAchievementsAssetFile()
    {
        Achievements asset = CustomAssetUtility.CreateAsset<Achievements>();
        asset.SheetName = "PJ_2nd_DBlist";
        asset.WorksheetName = "Achievements";
        EditorUtility.SetDirty(asset);        
    }
    
}