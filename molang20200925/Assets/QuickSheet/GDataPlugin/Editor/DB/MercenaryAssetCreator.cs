using UnityEngine;
using UnityEditor;
using System.IO;
using UnityQuickSheet;

///
/// !!! Machine generated code !!!
/// 
public partial class GoogleDataAssetUtility
{
    [MenuItem("Assets/Create/Google/Mercenary")]
    public static void CreateMercenaryAssetFile()
    {
        Mercenary asset = CustomAssetUtility.CreateAsset<Mercenary>();
        asset.SheetName = "PJ_2nd_DBlist";
        asset.WorksheetName = "Mercenary";
        EditorUtility.SetDirty(asset);        
    }
    
}