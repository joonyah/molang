using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using GDataDB;
using GDataDB.Linq;

using UnityQuickSheet;

///
/// !!! Machine generated code !!!
///
[CustomEditor(typeof(Manufacture))]
public class ManufactureEditor : BaseGoogleEditor<Manufacture>
{	    
    public override bool Load()
    {        
        Manufacture targetData = target as Manufacture;
        
        var client = new DatabaseClient("", "");
        string error = string.Empty;
        var db = client.GetDatabase(targetData.SheetName, ref error);	
        var table = db.GetTable<ManufactureData>(targetData.WorksheetName) ?? db.CreateTable<ManufactureData>(targetData.WorksheetName);
        
        List<ManufactureData> myDataList = new List<ManufactureData>();
        
        var all = table.FindAll();
        foreach(var elem in all)
        {
            ManufactureData data = new ManufactureData();
            
            data = Cloner.DeepCopy<ManufactureData>(elem.Element);
            myDataList.Add(data);
        }
                
        targetData.dataArray = myDataList.ToArray();
        
        EditorUtility.SetDirty(targetData);
        AssetDatabase.SaveAssets();
        
        return true;
    }
}
