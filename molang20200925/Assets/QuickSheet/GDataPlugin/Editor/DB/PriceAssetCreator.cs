using UnityEngine;
using UnityEditor;
using System.IO;
using UnityQuickSheet;

///
/// !!! Machine generated code !!!
/// 
public partial class GoogleDataAssetUtility
{
    [MenuItem("Assets/Create/Google/Price")]
    public static void CreatePriceAssetFile()
    {
        Price asset = CustomAssetUtility.CreateAsset<Price>();
        asset.SheetName = "PJ_2nd_DBlist";
        asset.WorksheetName = "Price";
        EditorUtility.SetDirty(asset);        
    }
    
}