using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using GDataDB;
using GDataDB.Linq;

using UnityQuickSheet;

///
/// !!! Machine generated code !!!
///
[CustomEditor(typeof(Merchant))]
public class MerchantEditor : BaseGoogleEditor<Merchant>
{	    
    public override bool Load()
    {        
        Merchant targetData = target as Merchant;
        
        var client = new DatabaseClient("", "");
        string error = string.Empty;
        var db = client.GetDatabase(targetData.SheetName, ref error);	
        var table = db.GetTable<MerchantData>(targetData.WorksheetName) ?? db.CreateTable<MerchantData>(targetData.WorksheetName);
        
        List<MerchantData> myDataList = new List<MerchantData>();
        
        var all = table.FindAll();
        foreach(var elem in all)
        {
            MerchantData data = new MerchantData();
            
            data = Cloner.DeepCopy<MerchantData>(elem.Element);
            myDataList.Add(data);
        }
                
        targetData.dataArray = myDataList.ToArray();
        
        EditorUtility.SetDirty(targetData);
        AssetDatabase.SaveAssets();
        
        return true;
    }
}
