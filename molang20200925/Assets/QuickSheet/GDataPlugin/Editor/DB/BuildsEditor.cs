using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using GDataDB;
using GDataDB.Linq;

using UnityQuickSheet;

///
/// !!! Machine generated code !!!
///
[CustomEditor(typeof(Builds))]
public class BuildsEditor : BaseGoogleEditor<Builds>
{	    
    public override bool Load()
    {        
        Builds targetData = target as Builds;
        
        var client = new DatabaseClient("", "");
        string error = string.Empty;
        var db = client.GetDatabase(targetData.SheetName, ref error);	
        var table = db.GetTable<BuildsData>(targetData.WorksheetName) ?? db.CreateTable<BuildsData>(targetData.WorksheetName);
        
        List<BuildsData> myDataList = new List<BuildsData>();
        
        var all = table.FindAll();
        foreach(var elem in all)
        {
            BuildsData data = new BuildsData();
            
            data = Cloner.DeepCopy<BuildsData>(elem.Element);
            myDataList.Add(data);
        }
                
        targetData.dataArray = myDataList.ToArray();
        
        EditorUtility.SetDirty(targetData);
        AssetDatabase.SaveAssets();
        
        return true;
    }
}
