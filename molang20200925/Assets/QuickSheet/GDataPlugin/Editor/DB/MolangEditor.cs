using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using GDataDB;
using GDataDB.Linq;

using UnityQuickSheet;

///
/// !!! Machine generated code !!!
///
[CustomEditor(typeof(Molang))]
public class MolangEditor : BaseGoogleEditor<Molang>
{	    
    public override bool Load()
    {        
        Molang targetData = target as Molang;
        
        var client = new DatabaseClient("", "");
        string error = string.Empty;
        var db = client.GetDatabase(targetData.SheetName, ref error);	
        var table = db.GetTable<MolangData>(targetData.WorksheetName) ?? db.CreateTable<MolangData>(targetData.WorksheetName);
        
        List<MolangData> myDataList = new List<MolangData>();
        
        var all = table.FindAll();
        foreach(var elem in all)
        {
            MolangData data = new MolangData();
            
            data = Cloner.DeepCopy<MolangData>(elem.Element);
            myDataList.Add(data);
        }
                
        targetData.dataArray = myDataList.ToArray();
        
        EditorUtility.SetDirty(targetData);
        AssetDatabase.SaveAssets();
        
        return true;
    }
}
