using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using GDataDB;
using GDataDB.Linq;

using UnityQuickSheet;

///
/// !!! Machine generated code !!!
///
[CustomEditor(typeof(ChestPer))]
public class ChestPerEditor : BaseGoogleEditor<ChestPer>
{	    
    public override bool Load()
    {        
        ChestPer targetData = target as ChestPer;
        
        var client = new DatabaseClient("", "");
        string error = string.Empty;
        var db = client.GetDatabase(targetData.SheetName, ref error);	
        var table = db.GetTable<ChestPerData>(targetData.WorksheetName) ?? db.CreateTable<ChestPerData>(targetData.WorksheetName);
        
        List<ChestPerData> myDataList = new List<ChestPerData>();
        
        var all = table.FindAll();
        foreach(var elem in all)
        {
            ChestPerData data = new ChestPerData();
            
            data = Cloner.DeepCopy<ChestPerData>(elem.Element);
            myDataList.Add(data);
        }
                
        targetData.dataArray = myDataList.ToArray();
        
        EditorUtility.SetDirty(targetData);
        AssetDatabase.SaveAssets();
        
        return true;
    }
}
