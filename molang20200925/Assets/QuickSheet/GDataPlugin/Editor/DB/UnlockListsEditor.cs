using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using GDataDB;
using GDataDB.Linq;

using UnityQuickSheet;

///
/// !!! Machine generated code !!!
///
[CustomEditor(typeof(UnlockLists))]
public class UnlockListsEditor : BaseGoogleEditor<UnlockLists>
{	    
    public override bool Load()
    {        
        UnlockLists targetData = target as UnlockLists;
        
        var client = new DatabaseClient("", "");
        string error = string.Empty;
        var db = client.GetDatabase(targetData.SheetName, ref error);	
        var table = db.GetTable<UnlockListsData>(targetData.WorksheetName) ?? db.CreateTable<UnlockListsData>(targetData.WorksheetName);
        
        List<UnlockListsData> myDataList = new List<UnlockListsData>();
        
        var all = table.FindAll();
        foreach(var elem in all)
        {
            UnlockListsData data = new UnlockListsData();
            
            data = Cloner.DeepCopy<UnlockListsData>(elem.Element);
            myDataList.Add(data);
        }
                
        targetData.dataArray = myDataList.ToArray();
        
        EditorUtility.SetDirty(targetData);
        AssetDatabase.SaveAssets();
        
        return true;
    }
}
