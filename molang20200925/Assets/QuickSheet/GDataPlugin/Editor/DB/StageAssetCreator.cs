using UnityEngine;
using UnityEditor;
using System.IO;
using UnityQuickSheet;

///
/// !!! Machine generated code !!!
/// 
public partial class GoogleDataAssetUtility
{
    [MenuItem("Assets/Create/Google/Stage")]
    public static void CreateStageAssetFile()
    {
        Stage asset = CustomAssetUtility.CreateAsset<Stage>();
        asset.SheetName = "PJ_2nd_DBlist";
        asset.WorksheetName = "Stage";
        EditorUtility.SetDirty(asset);        
    }
    
}