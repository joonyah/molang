using UnityEngine;
using UnityEditor;
using System.IO;
using UnityQuickSheet;

///
/// !!! Machine generated code !!!
/// 
public partial class GoogleDataAssetUtility
{
    [MenuItem("Assets/Create/Google/Merchant")]
    public static void CreateMerchantAssetFile()
    {
        Merchant asset = CustomAssetUtility.CreateAsset<Merchant>();
        asset.SheetName = "PJ_2nd_DBlist";
        asset.WorksheetName = "Merchant";
        EditorUtility.SetDirty(asset);        
    }
    
}