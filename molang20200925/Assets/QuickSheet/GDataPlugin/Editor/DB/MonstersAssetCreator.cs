using UnityEngine;
using UnityEditor;
using System.IO;
using UnityQuickSheet;

///
/// !!! Machine generated code !!!
/// 
public partial class GoogleDataAssetUtility
{
    [MenuItem("Assets/Create/Google/Monsters")]
    public static void CreateMonstersAssetFile()
    {
        Monsters asset = CustomAssetUtility.CreateAsset<Monsters>();
        asset.SheetName = "PJ_2nd_DBlist";
        asset.WorksheetName = "Monsters";
        EditorUtility.SetDirty(asset);        
    }
    
}