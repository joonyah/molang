using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using GDataDB;
using GDataDB.Linq;

using UnityQuickSheet;

///
/// !!! Machine generated code !!!
///
[CustomEditor(typeof(Enhance))]
public class EnhanceEditor : BaseGoogleEditor<Enhance>
{	    
    public override bool Load()
    {        
        Enhance targetData = target as Enhance;
        
        var client = new DatabaseClient("", "");
        string error = string.Empty;
        var db = client.GetDatabase(targetData.SheetName, ref error);	
        var table = db.GetTable<EnhanceData>(targetData.WorksheetName) ?? db.CreateTable<EnhanceData>(targetData.WorksheetName);
        
        List<EnhanceData> myDataList = new List<EnhanceData>();
        
        var all = table.FindAll();
        foreach(var elem in all)
        {
            EnhanceData data = new EnhanceData();
            
            data = Cloner.DeepCopy<EnhanceData>(elem.Element);
            myDataList.Add(data);
        }
                
        targetData.dataArray = myDataList.ToArray();
        
        EditorUtility.SetDirty(targetData);
        AssetDatabase.SaveAssets();
        
        return true;
    }
}
