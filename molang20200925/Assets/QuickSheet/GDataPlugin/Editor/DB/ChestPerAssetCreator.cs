using UnityEngine;
using UnityEditor;
using System.IO;
using UnityQuickSheet;

///
/// !!! Machine generated code !!!
/// 
public partial class GoogleDataAssetUtility
{
    [MenuItem("Assets/Create/Google/ChestPer")]
    public static void CreateChestPerAssetFile()
    {
        ChestPer asset = CustomAssetUtility.CreateAsset<ChestPer>();
        asset.SheetName = "PJ_2nd_DBlist";
        asset.WorksheetName = "ChestPer";
        EditorUtility.SetDirty(asset);        
    }
    
}