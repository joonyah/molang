using UnityEngine;
using UnityEditor;
using System.IO;
using UnityQuickSheet;

///
/// !!! Machine generated code !!!
/// 
public partial class GoogleDataAssetUtility
{
    [MenuItem("Assets/Create/Google/Builds")]
    public static void CreateBuildsAssetFile()
    {
        Builds asset = CustomAssetUtility.CreateAsset<Builds>();
        asset.SheetName = "PJ_Molang_DBlist";
        asset.WorksheetName = "Builds";
        EditorUtility.SetDirty(asset);        
    }
    
}