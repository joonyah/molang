using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using GDataDB;
using GDataDB.Linq;

using UnityQuickSheet;

///
/// !!! Machine generated code !!!
///
[CustomEditor(typeof(Monsters))]
public class MonstersEditor : BaseGoogleEditor<Monsters>
{	    
    public override bool Load()
    {        
        Monsters targetData = target as Monsters;
        
        var client = new DatabaseClient("", "");
        string error = string.Empty;
        var db = client.GetDatabase(targetData.SheetName, ref error);	
        var table = db.GetTable<MonstersData>(targetData.WorksheetName) ?? db.CreateTable<MonstersData>(targetData.WorksheetName);
        
        List<MonstersData> myDataList = new List<MonstersData>();
        
        var all = table.FindAll();
        foreach(var elem in all)
        {
            MonstersData data = new MonstersData();
            
            data = Cloner.DeepCopy<MonstersData>(elem.Element);
            myDataList.Add(data);
        }
                
        targetData.dataArray = myDataList.ToArray();
        
        EditorUtility.SetDirty(targetData);
        AssetDatabase.SaveAssets();
        
        return true;
    }
}
