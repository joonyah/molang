﻿using System.Collections;
using UnityEngine;

public interface ColorSetterInterface
{
    void Refresh();

    void SetColor(float time);
}

[ExecuteInEditMode]
public class LightColorController : MonoBehaviour
{
    [SerializeField] [Range(0,1)] float time;

    private ColorSetterInterface[] setters;
    private float currentTime;
    private Coroutine co = null;

    public float timeValue => currentTime;

    public void GetSetters()
    {
        setters = GetComponentsInChildren<ColorSetterInterface>();
        foreach (var setter in setters)
            setter.Refresh();
    }

    private void OnEnable()
    {
        time = 0;
        GetSetters();
        UpdateSetters();
    }

    private void OnDisable()
    {
        time = 0;
        UpdateSetters();
    }

    
    private void Update()
    {
        if (currentTime != time)
            UpdateSetters();
    }
    

    public void UpdateSetters()
    {
        currentTime = time;

        foreach (var setter in setters)
            setter.SetColor(time);
    }

    IEnumerator SmoothSteppin(float duration, float fromValue, float ToValue)
    {
        var timer = 0f;
        while (timer <= duration)
        {
            //Count the other direction.  Its simpler. Or you can do (10-timer) in your
            //SmoothStep Calculation
            time = Mathf.SmoothStep(fromValue, ToValue, timer / duration);
            //Divide by 10 to put the timer back in the range of SmoothStep.
            timer += Time.deltaTime;

            UpdateSetters();

            yield return null;
        }
    }

    public void SetTime(float newTime, float duration)
    {
        if (co != null)
        {
            StopCoroutine(co);            
        }
        co = StartCoroutine(SmoothSteppin(duration, time, newTime));
    }

    public void SetTime(float newTime)
    {
        time = newTime;
        UpdateSetters();
    }




    public float GetTime()
    {
        return time;
    }
        
}
