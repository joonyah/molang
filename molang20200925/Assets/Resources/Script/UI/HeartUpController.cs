﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeartUpController : MonoBehaviour
{
    SpriteRenderer image;
    public void InitSetting()
    {
        image = GetComponent<SpriteRenderer>();
        StartCoroutine(UpdateCoroutine());
    }

    IEnumerator UpdateCoroutine()
    {
        float a = 1.0f;
        while(true)
        {
            image.color = new Color(1, 1, 1, a);
            a -= Time.fixedDeltaTime / 2;
            transform.Translate(Vector3.up * 2 * Time.fixedDeltaTime);
            if (a <= 0.0f) break;
            yield return new WaitForFixedUpdate();
        }
        Destroy(gameObject);
    }
}
