﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Score : MonoBehaviour
{
    public UIController uiControl;
    public int nMolangCount = 0;
    public int nBuildCount = 0;
    public int nHeartCount = 0;
    public int nStarCount = 0;
    public int nLightCount = 0;
    public int nCloverCount = 0;


    private void Start()
    {
        InvokeRepeating("UpdateInvoke", 0, 1);
    }

    void UpdateInvoke()
    {
        StartCoroutine(CountUpdate(nHeartCount, nHeartCount + (nMolangCount * 7), 1));
        StartCoroutine(CountUpdate(nStarCount, nStarCount + (nBuildCount * 15), 2));
        if (uiControl)
            uiControl.BuildStepCheck(nHeartCount + (nMolangCount * 7));
    }

    IEnumerator CountUpdate(int prev, int next, int num)
    {
        int diff = next - prev;
        int amount = Mathf.CeilToInt((float)diff / 30.0f);
        if (amount > 0)
        {
            for (int i = prev; i <= next; i += amount)
            {

                if (num == 1)
                {
                    if (uiControl)
                        uiControl.HeartCountText.text = ChangeUnit(i);
                    nHeartCount = i;
                }
                else if (num == 2)
                {
                    if (uiControl)
                        uiControl.StarCountText.text = ChangeUnit(i);
                    nStarCount = i;
                }
                yield return new WaitForFixedUpdate();
            }
        }
    }

    public string ChangeUnit(float _price, int _level = 0)
    {
        string returnText = _price + LevelTextChange(_level);
        if (_price >= 1000.0f)
        {
            _price /= 1000;
            _level++;
            returnText = ChangeUnit((float)(Mathf.FloorToInt(_price * 10) / 10.0f), _level);
        }
        return returnText;
    }
    string LevelTextChange(int _level)
    {
        switch (_level)
        {
            case 0:
                return "a";
            case 1:
                return "b";
            case 2:
                return "c";
            case 3:
                return "d";
            case 4:
                return "e";
            case 5:
                return "f";
            case 6:
                return "g";
            case 7:
                return "h";
            case 8:
                return "i";
            case 9:
                return "j";
            case 10:
                return "k";
            case 11:
                return "l";
            case 12:
                return "m";
            case 13:
                return "n";
            case 14:
                return "o";
            case 15:
                return "p";
            case 16:
                return "q";
            case 17:
                return "r";
            case 18:
                return "s";
            case 19:
                return "t";
            case 20:
                return "u";
            case 21:
                return "v";
            case 22:
                return "w";
            case 23:
                return "x";
            case 24:
                return "y";
            case 25:
                return "z";
            default:
                return "a";
        }
    }
}
