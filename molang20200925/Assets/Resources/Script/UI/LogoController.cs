﻿using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogoController : MonoBehaviour
{
    //
    public SkeletonAnimation anim;

    //상단 UI
    public Animation TopUiAnim;
    //하단 UI
    public Animation BottomUiAnim;
    //상단 정보 UI
    public GameObject TopSecondObj;

    //화면 클릭 확인 여부
    bool isTouch = false;

    //
    private System.Action<bool> logoCallback;
    private Spine.TrackEntry logoTrack;


    void Start()
    {
        isTouch = false;
        anim.state.SetAnimation(0, "appear", false);
    }

    public void LogoEnd(System.Action<bool> _callback)
    {
        logoCallback = _callback;
    }

    public void EndAnimation(Spine.TrackEntry _te)
    {
        //상 하단 UI 출력
        TopUiAnim.enabled = true;
        TopUiAnim.Play();
        BottomUiAnim.enabled = true;
        BottomUiAnim.Play();

        //튜토 클리어 상태 체크
        Invoke("TutoObj", 1f);
        
        //0.5 초후 상단 정보 UI출력
        Invoke("ShowObj", 0.5f);
    }

    private void TutoObj()
    {
        //
        logoCallback(true);
    }

    private void ShowObj()
    {
        //logo 오브젝트 off
        this.gameObject.SetActive(false);

        TopSecondObj.SetActive(true);
    }

    public void ClickEvent()
    {
        if (!isTouch)
        {
            //
            isTouch = true;

            //
            logoTrack = anim.state.AddAnimation(0, "disappear", false, 0f);
            logoTrack.Complete += EndAnimation;               
        }
    }
}
