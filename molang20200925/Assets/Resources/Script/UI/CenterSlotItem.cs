﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CenterSlotItem : MonoBehaviour
{
    public string strItemId;
    public UI2DSprite itemIcon;
    public UILabel levelText;
    public UILabel nameText;
    public UILabel amountText;
    public UILabel priceText;
    public UI2DSprite priceIcon;

    public void InitSetting(object _item, Sprite _sprite, int _type)
    {
        if (_item == null) return;

        itemIcon.sprite2D = _sprite;
        itemIcon.MakePixelPerfect();
        itemIcon.transform.localScale = new Vector3(0.15f, 0.15f, 0.15f);
        if (_type == 0)
        {
            TestMakeItemInformation makeItem = (TestMakeItemInformation)_item;
            levelText.text = "LV." + makeItem.nowLevel.ToString();
            nameText.text = makeItem.name;
            amountText.text = "행복지수 " + ChangeUnit(makeItem.amount) + "증가";
            priceText.text = ChangeUnit(makeItem.price);
        }
        else if (_type == 1)
        {
            TestBuildUpgradeInformation buildItem = (TestBuildUpgradeInformation)_item;
            levelText.text = "LV." + buildItem.nowLevel.ToString();
            nameText.text = buildItem.name;
            amountText.text = "행복지수 " + ChangeUnit(buildItem.amount) + "증가";
            priceText.text = ChangeUnit(buildItem.price);
        }
    }

    string ChangeUnit(float _price, int _level = 0)
    {
        string returnText = _price + LevelTextChange(_level);
        if (_price >= 1000.0f)
        {
            _price /= 1000;
            _level++;
            returnText = ChangeUnit((float)(Mathf.FloorToInt(_price * 10) / 10.0f), _level);
        }
        return returnText;
    }
    string LevelTextChange(int _level)
    {
        switch(_level)
        {
            case 0:
                return "a";
            case 1:
                return "b";
            case 2:
                return "c";
            case 3:
                return "d";
            case 4:
                return "e";
            case 5:
                return "f";
            case 6:
                return "g";
            case 7:
                return "h";
            case 8:
                return "i";
            case 9:
                return "j";
            case 10:
                return "k";
            case 11:
                return "l";
            case 12:
                return "m";
            case 13:
                return "n";
            case 14:
                return "o";
            case 15:
                return "p";
            case 16:
                return "q";
            case 17:
                return "r";
            case 18:
                return "s";
            case 19:
                return "t";
            case 20:
                return "u";
            case 21:
                return "v";
            case 22:
                return "w";
            case 23:
                return "x";
            case 24:
                return "y";
            case 25:
                return "z";
            default:
                return "a";
        }
    }

    public void ButtonClickEvent()
    {
    }

}
