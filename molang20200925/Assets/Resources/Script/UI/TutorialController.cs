﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum TagTabs { ControlTab, CollectionTab }

public class TutorialController : MonoBehaviour
{
    public UIController uiControl;
    public int Step = 0;

    public GameObject AlphaObj;
    public Transform MovingArrow;
    public GameObject MovingArrowObj;
    public GameObject ContentObj;
    public UILabel ContentLabel;
    public float fMoveSpeed;

    public GameObject TutoMolangObj;

    public GameObject ControlBooksObj;
    public GameObject CollectionBooksObj;
    public GameObject BooksInfoObj;
    public GameObject BooksQuestionObj;
    public GameObject[] TabWalls;

    public UIPanel StepPanel;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(ArrowMovingCoroutine());
    }
    void OnEnable()
    {
        AlphaObj.SetActive(true);
        uiControl.cameraMove.isCameraMove = false;
        if (Step == 1)
        {
            Step++;
            uiControl.fCameraMinZoom = 70;
            StartCoroutine(uiControl.MolangSpawnEvent(new Vector3(-65500, 54400, -50)));
            ContentLabel.text = "충분한 재화가 모였네요\n재화가 충족되면 색이 바뀐 아이콘을\n눌러 건물을 지을 수 있어요";
            //ContentLabel.text = "몰랑이를 위해 아름다운\n건물을 만들어 주세요";
            ContentObj.SetActive(true);
        }
    }
    IEnumerator ArrowMovingCoroutine()
    {
        float y = 0.0f;
        float minY = -5.0f;
        float maxY = 5.0f;
        bool isUpDown = false;
        while (true)
        {
            if (isUpDown)
                y -= Time.fixedDeltaTime * fMoveSpeed;
            else
                y += Time.fixedDeltaTime * fMoveSpeed;
            MovingArrow.localPosition = new Vector3(0, y, 0);
            if (y < minY)
            {
                isUpDown = !isUpDown;
                y = minY;
            }
            else if (y > maxY)
            {
                isUpDown = !isUpDown;
                y = maxY;
            }
            yield return new WaitForFixedUpdate();
        }
    }
    public void MolangClickEvent(GameObject _obj)
    {
        _obj.SetActive(false);
        TabChange(null);
        TutoMolangObj.SetActive(true);
        ContentObj.SetActive(false);
        if (Step == 0)
        {
            for (int i = 0; i < TabWalls.Length; i++)
            {
                if (i == Step) TabWalls[i].SetActive(false);
                else TabWalls[i].SetActive(true);
            }
            Step++;
            MovingArrowObj.transform.localPosition = new Vector3(-41, 422, 0);
        }
        else if (Step == 1)
        {
            for (int i = 0; i < TabWalls.Length; i++)
            {
                if (i == Step) TabWalls[i].SetActive(false);
                else TabWalls[i].SetActive(true);
            }
            if (Step == 1) MovingArrowObj.transform.localPosition = new Vector3(200, 315);
            ControlBooksObj.transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
            Step++;
        }
    }
    public void TabChange(GameObject _obj)
    {
        if (_obj == null)
        {
            CollectionBooksObj.SetActive(false);
            ControlBooksObj.SetActive(true);
            return;
        }
        TagTabs paramaterEnum = (TagTabs)Enum.Parse(typeof(TagTabs), _obj.name);
        switch (paramaterEnum)
        {
            case TagTabs.ControlTab:
                CollectionBooksObj.SetActive(false);
                ControlBooksObj.SetActive(true);
                break;
            case TagTabs.CollectionTab:
                MovingArrowObj.transform.localPosition = new Vector3(-185, 310);
                ControlBooksObj.SetActive(false);
                CollectionBooksObj.SetActive(true);
                break;
        }
    }
    public void ObjCloseEvent(GameObject _closeObj)
    {
        _closeObj.SetActive(false);
    }
    public void ObjOpenEvent(GameObject _openObj)
    {
        if (Step == 1) MovingArrowObj.transform.localPosition = new Vector3(5, -30);
    }
    public void MolangCreateTutorialEnd()
    {
        BooksInfoObj.SetActive(false);
        BooksQuestionObj.SetActive(false);
        TutoMolangObj.SetActive(false);
        MovingArrowObj.SetActive(false);
        ContentLabel.text = "잘하셨어요!\n몰랑이가 증가하면 더 많은\n활력과 행복을 얻을 수 있답니다";
        ContentObj.transform.localPosition = new Vector3(0, 290, 0);
        ContentObj.transform.Find("ClearBtn").gameObject.SetActive(true);
        ContentObj.SetActive(true);
    }
    public void TutorialEnd()
    {
        uiControl.StepChange();
        if (Step == 1)
        {
            AlphaObj.SetActive(false);
            ContentObj.SetActive(false);
            uiControl.cameraMove.isCameraMove = true;
            gameObject.SetActive(false);
        }
        else if (Step == 2)
        {
            ContentLabel.text = "몰랑이를 위해 아름다운\n건물을 만들어 주세요";
            uiControl.cameraMove.isCameraMove = false;
            Step++;
        }
        else if (Step == 3)
        {
            AlphaObj.SetActive(false);
            uiControl.cameraMove.isCameraMove = false;
            ContentObj.SetActive(false);
            MovingArrowObj.transform.localPosition = new Vector3(100, 295);
            MovingArrowObj.SetActive(true);
            StepPanel.depth = 2;
            StartCoroutine(ArrowMovingCoroutine());
        }
    }
    public void SaveCollectionMolang()
    {
        TutoMolangObj.SetActive(false);
    }
    public void MolangSpawnEventStep(int _step)
    {
        if(_step == 0)
        {
            if (Step == 1) AlphaObj.SetActive(false);
            MovingArrowObj.SetActive(false);
        }
        else if(_step == 1)
        {
            if (Step == 1)
            {
                Invoke("MolangSpawnEventStep2", 4.0f);
            }
        }
    }
    void MolangSpawnEventStep2()
    {
        AlphaObj.SetActive(true);
        uiControl.cameraMove.isCameraMove = true;
        MolangCreateTutorialEnd();
    }
}
