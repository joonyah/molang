﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HallOfFameController : MonoBehaviour
{
    public DynamicGrid dGrid;
    public GameObject[] ScrollviewObjs;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OpenScrollView(GameObject _obj)
    {
        for(int i=0; i< ScrollviewObjs.Length; i++)
        {
            if (ScrollviewObjs[i] == _obj)
            {
                if (ScrollviewObjs[i].activeSelf)
                    ScrollviewObjs[i].SetActive(false);
                else
                    ScrollviewObjs[i].SetActive(true);
            }
            else ScrollviewObjs[i].SetActive(false);
        }
        dGrid.enabled = true;
    }
}
