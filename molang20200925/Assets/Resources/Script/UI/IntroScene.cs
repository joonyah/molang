﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.SceneManagement;

//google
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using System.Threading.Tasks;

//ads
using GoogleMobileAds.Api;

public class IntroScene : MonoBehaviour
{
    //씬 넘버링
    private enum SCENE
    {
        INTRO = 0,
        GAME,
        UI
    }

    //퍼센트 표시를 위한(Debug 용)
    public UILabel percentLabel;

    //ADS 상태값를 위한(Debug 용)
    public UILabel adsLabel;

    //하단 로딩바
    public UI2DSprite loadingBar;

    [Header("NAVER CAFE ID")]
    public int CafeId = 30220002;

    [Header("NAVER CAFE ClientId")]
    public string NaverLoginClientId = "MORNOZaW5Yw0CyeoB3lW";

    [Header("NAVER CAFE ClientSecret")]
    public string NaverLoginClientSecret = "RAQU2txBr6";

    //
    [Header("GOOGLE ADS")]
    public static RewardedAd rewardedAd;
    public static string adUnitId = "";
    public static string strRetrun = "";

    //
    [Header("GOOGLE PLAY")]
    public string authCode = "";

    void Start()
    {
        //구글 로그인 설정
        LoginInit();

        //ADS 설정
        ADSInit();

        //네이커 카페 설정
        //NAVERCafeInit();
    }

    void Update()
    {
        //Debug 용 View 업데이트
        adsLabel.text = strRetrun;
    }

    private void LoginInit()
    {
        //구글 로그인 설정
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
               .RequestServerAuthCode(false)
               .Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();

        LoginTest();
    }

    private void LoginTest()
    {
        Social.localUser.Authenticate((bool success) =>
        {
            if (success)
            {
                authCode = PlayGamesPlatform.Instance.GetServerAuthCode();
                percentLabel.text = authCode;
                //Debug.Log("GetIdToken : " + PlayGamesPlatform.Instance.GetIdToken());

                Firebase.Auth.FirebaseAuth auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
                Firebase.Auth.Credential credential = Firebase.Auth.PlayGamesAuthProvider.GetCredential(authCode);
                auth.SignInWithCredentialAsync(credential).ContinueWith(task =>
                {
                    if (task.IsCanceled)
                    {
                        //percentLabel.text = "SignInWithCredentialAsync was canceled.";
                        Debug.LogError("SignInWithCredentialAsync was canceled.");
                        return;
                    }
                    if (task.IsFaulted)
                    {
                        //percentLabel.text = "SignInWithCredentialAsync encountered an error: " + task.Exception;
                        Debug.LogError("SignInWithCredentialAsync encountered an error: " + task.Exception);
                        return;
                    }

                    Firebase.Auth.FirebaseUser newUser = task.Result;
                    Debug.LogFormat("User signed in successfully: {0} ({1})",
                        newUser.DisplayName, newUser.UserId);
                });

                Firebase.Auth.FirebaseUser user = auth.CurrentUser;
                if (user != null)
                {
                    string playerName = user.DisplayName;

                    // The user's Id, unique to the Firebase project.
                    // Do NOT use this value to authenticate with your backend server, if you
                    // have one; use User.TokenAsync() instead.
                    string uid = user.UserId;

                    percentLabel.text = "uid : " + uid + "\nplayerName : " + playerName;
                }
            }
        });
    }

    IEnumerator LoadGameScene()
    {
        //Scene Load mode 
        //LoadSceneMode.Single : 기존 로드된 모든씬을 종료하고 지정된 씬을 로드한다.
        //LoadSceneMode.Additive : 현재 로드된 씬에 지정한 씬을 추가 로드 한다.      
        AsyncOperation loadScene = SceneManager.LoadSceneAsync((int)SCENE.GAME, LoadSceneMode.Single);
        SceneManager.LoadScene((int)SCENE.UI, LoadSceneMode.Additive);

        loadScene.allowSceneActivation = false;
        float timer = 0.0f, fLoadValue = 0.0f;
        loadingBar.fillAmount = 0f;

        while (true)
        {
            if (loadScene.isDone)
            {
                //Debug.Log("done ");
                loadScene.allowSceneActivation = true;
                yield break;
            }
            else
            {
                timer += Time.deltaTime;
                fLoadValue = Mathf.Lerp(fLoadValue, 1f, timer);
                loadingBar.fillAmount = fLoadValue;
                percentLabel.text = (fLoadValue * 100f).ToString() + "%";

                if (loadScene.progress < 0.9f)
                {
                    if (fLoadValue >= loadScene.progress)
                    {
                        timer = 0f;
                        loadingBar.fillAmount = 1f;
                    }
                }
            }
            yield return null;
        }
    }

    private void NAVERCafeInit()
    {
        //if (!Application.isEditor)
            //GLink.sharedInstance().init(CafeId, NaverLoginClientId, NaverLoginClientSecret);
    }

    public void BtnCafe()
    {
        if (!Application.isEditor)
        {
            //GLink.sharedInstance().setWidgetStartPosition(false, 60);
            //GLink.sharedInstance().executeHome();
        }
    }

    private void ADSInit()
    {
        MobileAds.Initialize(initStatus =>
        {
            if (initStatus != null)
            {
                ADSSetting();
            }
        });
    }

    private static void ADSSetting()
    {
        //구글 ads
        //adUnitId = "ca-app-pub-6628079170157281/9137492998";
        adUnitId = "ca-app-pub-3940256099942544/5224354917";

        // Initialize the Google Mobile Ads SDK.
        rewardedAd = new RewardedAd(adUnitId);

        if (rewardedAd == null) return;

        AdRequest request = new AdRequest.Builder().Build();
        rewardedAd.LoadAd(request);

        ADSCallbackSet();
    }

    private static void ADSCallbackSet()
    {
        // 광고 로드가 완료될 때 실행됩니다.
        rewardedAd.OnAdLoaded += HandleRewardedAdLoaded;
        // 광고 로드가 실패할 경우 실행됩니다.
        rewardedAd.OnAdFailedToLoad += HandleRewardedAdFailedToLoad;

        // 광고 중일때 실행 됩니다.
        rewardedAd.OnAdOpening += HandleRewardedAdOpening;
        // 광고 표시 실패시 실행 됩니다.
        rewardedAd.OnAdFailedToShow += HandleRewardedAdFailedToShow;

        // 사용자가 동영상 시청에 대한 보상을 받아야 할 때 실행됩니다. 
        rewardedAd.OnUserEarnedReward += HandleUserEarnedReward;
        // 사용자가 닫기 아이콘을 탭하거나 뒤로 버튼을 사용하여 보상형 동영상 광고를 닫을 때 실행됩니다.
        rewardedAd.OnAdClosed += HandleRewardedAdClosed;
    }

    public void AdsTest()
    {
        Debug.Log(" ADS Click ");
        if (rewardedAd.IsLoaded())
        {
            rewardedAd.Show();
        }
    }

    public static void HandleRewardedAdLoaded(object sender, EventArgs args) { strRetrun = "HandleRewardedAdLoaded event received"; }
    public static void HandleRewardedAdFailedToLoad(object sender, EventArgs args){ strRetrun = "HandleRewardedAdFailedToLoad event received";}
    public static void HandleRewardedAdOpening(object sender, EventArgs args){strRetrun = "HandleRewardedAdOpening event received"; }
    public static void HandleRewardedAdFailedToShow(object sender, AdErrorEventArgs args) { strRetrun = "HandleRewardedAdFailedToShow event received: " + args.Message; }
    public static void HandleRewardedAdClosed(object sender, EventArgs args) { strRetrun = "HandleRewardedAdClosed event received"; ADSSetting();}
    public static void HandleUserEarnedReward(object sender, Reward args) { strRetrun = "HandleRewardedAdRewarded : " + args.Amount.ToString() + " " + args.Type;  }
}
