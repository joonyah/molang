﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DynamicGrid : MonoBehaviour
{
    [DoNotObfuscateNGUI]
    public enum Arrangement
    {
        Horizontal,
        Vertical
    }

    public Arrangement arrangement = Arrangement.Horizontal;
    public List<Transform> ChildList = new List<Transform>();
    public bool isScrollViewAnim = false;
    public int AnimSpeed = 20;
    public bool isScrollViewAnimWait = false;
    public bool isScrollViewAnimWait2 = false;

    // Start is called before the first frame update
    void Start()
    {
        SortSetting();
        enabled = false;
    }
    private void OnEnable()
    {
        SortSetting();
        enabled = false;
    }

    void SortSetting()
    {
        if (ChildList.Count > 0) ChildList.Clear();
        int t = transform.childCount;
        for (int i = 0; i < t; i++)
        {
            if (transform.GetChild(i).gameObject.activeSelf)
                ChildList.Add(transform.GetChild(i));
        }

        for (int i = 0; i < ChildList.Count; i++)
        {
            ChildList[i].transform.localPosition = GetPosition(i);
        }
        if (isScrollViewAnim)
            isScrollViewAnimWait = false;
    }

    Vector3 GetPosition(int _index)
    {
        if (_index == 0) return Vector3.zero;

        Transform _obj = ChildList[_index - 1];
        if (_obj.GetComponent<UI2DSprite>())
        {
            UI2DSprite s = _obj.GetComponent<UI2DSprite>();
            Vector3 size = s.localSize;

            if (arrangement == Arrangement.Horizontal)
                return new Vector3(ChildList[_index - 1].localPosition.x + size.x, 0, 0);
            else if (arrangement == Arrangement.Vertical)
                return new Vector3(0, ChildList[_index - 1].localPosition.y - size.y, 0);
        }
        else if (_obj.GetComponent<UIPanel>())
        {
            UIPanel s = _obj.GetComponent<UIPanel>();
            Vector3 size = s.GetViewSize();

            if(isScrollViewAnim && !isScrollViewAnimWait2)
            {
                isScrollViewAnimWait2 = true;
                isScrollViewAnimWait = true;
                StartCoroutine(ScrollViewDownAnim(s));
                s.clipOffset = Vector2.zero;
                s.baseClipRegion = new Vector4(s.finalClipRegion.x, s.finalClipRegion.y, s.finalClipRegion.z, 0);
            }

            if (arrangement == Arrangement.Horizontal)
                return new Vector3(ChildList[_index - 1].localPosition.x + size.x, 0, 0);
            else if (arrangement == Arrangement.Vertical)
                return new Vector3(0, ChildList[_index - 1].localPosition.y - size.y, 0);
        }
        return Vector3.zero;
    }

    IEnumerator ScrollViewDownAnim(UIPanel s)
    {
        Vector4 endSize = s.finalClipRegion;
        Vector3 endOffset = new Vector3(0, -(endSize.w / 2));
        Vector2 center = new Vector2(s.baseClipRegion.x, s.baseClipRegion.y);
        while (isScrollViewAnimWait)
            yield return null;
        float offSetEndY = endOffset.y;
        float sizeEndY = endSize.w;

        int maxCount = AnimSpeed;
        int nowCount = 0;
        float offSetAmountValue = offSetEndY / (float)maxCount;
        float sizeAmountValue = sizeEndY / (float)maxCount;
        while (true)
        {
            s.clipOffset = new Vector2(0, s.clipOffset.y + offSetAmountValue);
            s.baseClipRegion = new Vector4(center.x, center.y, endSize.z, s.finalClipRegion.w + sizeAmountValue);
            nowCount++;
            SortSetting();
            if (nowCount >= maxCount) break;
            yield return null;
        }
        isScrollViewAnimWait2 = false;
    }
}
