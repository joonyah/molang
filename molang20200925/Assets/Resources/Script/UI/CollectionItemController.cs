﻿using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CollectionItemController : MonoBehaviour
{
    [Header("정보")]
    public string strId;
    public string strName;
    public GameObject InfoObj;
    public GameObject LockObj;
    public UIController uiController;
    public bool isLock = true;

    [Header("튜토전용")]
    public bool isTutorial = false;
    public GameObject ListObj;
    public GameObject ArrowObj;


    [Header("오픈정보")]
    public List<Sprite> Sprites = new List<Sprite>();
    public List<GameObject> MotionList = new List<GameObject>();
    public List<UI2DSprite> MotionSpriteList = new List<UI2DSprite>();
    public UI2DSprite TitleIconSprite;
    public GameObject MakeLockObj;
    public GameObject GiftLockObj;
    public UILabel nameText;
    public UILabel ContText;

    void Start()
    {
        if (isTutorial)
        {
            ListObj = transform.parent.parent.gameObject;
            ArrowObj = transform.parent.parent.parent.parent.Find("Default").Find("Arrow").gameObject;
        }
        uiController = FindObjectOfType(typeof(UIController)) as UIController;
        InfoObj = uiController.transform.Find("UIPanel").Find("BooksInfomation").gameObject;


        LockObj = transform.Find("Lock").gameObject;
        MakeLockObj = InfoObj.transform.Find("Background").Find("BarGroup").Find("MakeBar").Find("Lock").gameObject;
        GiftLockObj = InfoObj.transform.Find("Background").Find("BarGroup").Find("GiftBar").Find("Lock").gameObject;
        nameText = InfoObj.transform.Find("Background").Find("TextGroup").Find("NameText").GetComponent<UILabel>();
        ContText = InfoObj.transform.Find("Background").Find("TextGroup").Find("ContentText").GetComponent<UILabel>();

        Sprite[] tempSprites = Resources.LoadAll<Sprite>("Icon/motion");
        for (int i = 0; i < tempSprites.Length; i++)
        {
            if (tempSprites[i].name.Contains(strId)) Sprites.Add(tempSprites[i]);
        }

        UI2DSprite[] tempArray = InfoObj.transform.Find("Background").Find("MotionGroup").Find("Grid").GetComponentsInChildren<UI2DSprite>(true);
        for (int i = 0; i < tempArray.Length; i++)
        {
            if (tempArray[i].name.Equals("Motion")) MotionList.Add(tempArray[i].gameObject);
            else MotionSpriteList.Add(tempArray[i]);
        }
        TitleIconSprite = InfoObj.transform.Find("Background").Find("MolangIcon").Find("Icon").GetComponent<UI2DSprite>();

        if (isLock) LockObj.SetActive(true);
        else LockObj.SetActive(false);
    }
    public void BtnClickEvent()
    {
        uiController.strSpawnMolangID = strId;
        if (isTutorial)
        {
            ArrowObj.transform.localPosition = new Vector3(5, 208);
            ListObj.SetActive(false);
        }
        for (int i = 0; i < MotionList.Count; i++)
            MotionList[i].SetActive(false);
        for (int i = 0; i < Sprites.Count; i++)
        {
            string[] tempName = Sprites[i].name.Split('_');
            string animName = string.Empty;
            if(tempName.Length > 1)
            {
                for (int j = 1; j < tempName.Length; j++)
                {
                    if (j != 1)
                        animName += "_";
                    animName += tempName[j];
                }
            }
            MotionSpriteList[i].sprite2D = Sprites[i];
            MotionList[i].SetActive(true);
            if (MotionList[i].GetComponent<MotionCardEvent>())
                MotionList[i].GetComponent<MotionCardEvent>().motionName = animName;
            if (MotionList[i].GetComponent<MotionCardEvent>())
                MotionList[i].GetComponent<MotionCardEvent>().motionSkin = uiController.GetSkinName(strId);

            if (Sprites[i].name.Contains("idle")) TitleIconSprite.sprite2D = Sprites[i];
        }
        nameText.text = strName;
        if (isLock) MakeLockObj.SetActive(true);
        else MakeLockObj.SetActive(false);
        InfoObj.SetActive(true);
    }
}
