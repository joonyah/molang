﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System;

[Serializable]
public struct TabMenu
{
    public string[] titles;
}

public class CenterFrameManager : MonoBehaviour
{
    public static CenterFrameManager instance;

    [SerializeField] private GameObject openObj;
    [Header("LeftTitle")]
    [SerializeField] private Sprite[] FrameSprites;
    [SerializeField] private string[] FrameTitleTexts;
    [SerializeField] private Image LeftTitleImage;
    [SerializeField] private TextMeshProUGUI LeftTitleText;
    [SerializeField] private TextMeshProUGUI[] Tabs;
    [SerializeField] private int[] nTabCounts;
    [SerializeField] private TabMenu[] strTabTitles;
    [SerializeField] private TMP_FontAsset FontAssetBold;
    [SerializeField] private TMP_FontAsset FontAssetBold_outline;
    [SerializeField] private TMP_FontAsset FontAssetNormal;

    [Header("BottomPanel")]
    [SerializeField] private GameObject MakeObj;
    [SerializeField] private GameObject MakePanel;

    [Header("CenterSlot")]
    [SerializeField] private GameObject CenterSlotPrefabs;
    [SerializeField] private Transform CenterSlotParent;
    List<GameObject> objList = new List<GameObject>();

    public GameObject[] BottomObjSlots;
    public Image CrossHairImage;
    public bool isPick = false;
    public int BottomMinNum = 0;

    public AstarPath Astar;

    Coroutine CloseCoroutine = null;
    private void Awake()
    {
        if (instance == null) instance = this;
    }
    void Start()
    {
        
    }
    void Update()
    {
        if(isPick)
        {
            CrossHairImage.transform.position = Input.mousePosition;
        }
    }
    public void ShowFrame(int type)
    {
        if (type == 5)
        {
            GetComponent<Image>().enabled = true;
            MakeObj.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, 0, 0);
            MakeObj.SetActive(true);
            for (int i = 0; i < BottomObjSlots.Length; i++) BottomObjSlots[i].SetActive(false);
            SaveItem[] loadItems = SaveAndLoadManager.instance.LoadItemAll();
            if (loadItems != null)
            {
                if (loadItems.Length > 0)
                {
                    int count = 0;
                    for (int i = BottomMinNum; i < BottomMinNum + BottomObjSlots.Length; i++)
                    {
                        if (i >= loadItems.Length) break;
                        if (loadItems[i].type == 0)
                        {
                            BuildsData bData = Array.Find(SheetManager.instance.BuildDB.dataArray, item => item.ID.Equals(loadItems[i].id));
                            BottomObjSlots[count].transform.Find("Text").GetComponent<TextMeshProUGUI>().text = bData.NAME;
                            BottomObjSlots[count].transform.Find("Icon").GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/" + bData.TYPE + "/" + bData.ID);
                            BottomObjSlots[count].transform.Find("Count").GetComponent<TextMeshProUGUI>().text = loadItems[i].count.ToString();
                        }
                        else if (loadItems[i].type == 1)
                        {
                            MakeData mData = Array.Find(SheetManager.instance.MakeDB.dataArray, item => item.ID.Equals(loadItems[i].id));
                            BottomObjSlots[count].transform.Find("Text").GetComponent<TextMeshProUGUI>().text = mData.NAME;
                            BottomObjSlots[count].transform.Find("Icon").GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/" + mData.TYPE + "/" + mData.ID);
                            BottomObjSlots[count].transform.Find("Count").GetComponent<TextMeshProUGUI>().text = loadItems[i].count.ToString();

                        }
                        BottomObjSlots[count].SetActive(true);
                        count++;
                    }
                }
            }
        }
        else if (type == -1)
        {
            WorldManager.instance.StartCoroutine(WorldManager.instance.CloseUIEvent(true));
        }
        else
        {
            GetComponent<Image>().enabled = true;
            ChangeFrame(type);
            openObj.SetActive(true);
        }
    }
    public void CloseFrame()
    {
        if (CloseCoroutine == null)
            CloseCoroutine = StartCoroutine(CloseFrameCoroutine());
    }
    IEnumerator CloseFrameCoroutine()
    {
        yield return new WaitForSeconds(0.25f);
        CloseCoroutine = null;
        GetComponent<Image>().enabled = false;
        openObj.SetActive(false);
        MakeObj.SetActive(false);
    }
    public void CloseMake()
    {
        MakePanel.SetActive(false);
        MakeObj.SetActive(true);
    }
    public void ChangeFrame(int type)
    {
        if (type == -1) return;
        for (int i = 0; i < objList.Count; i++)
        {
            if (objList[i])
            {
                Destroy(objList[i]);
                objList.RemoveAt(i);
                i--;
                continue;
            }
        }
        objList.Clear();

        LeftTitleImage.sprite = FrameSprites[type];
        LeftTitleImage.GetComponent<RectTransform>().sizeDelta = new Vector2(FrameSprites[type].bounds.size.x * 80.0f, FrameSprites[type].bounds.size.y * 80.0f);
        LeftTitleText.text = FrameTitleTexts[type];

        for (int i = 0; i < Tabs.Length; i++)
        {
            Tabs[i].transform.parent.gameObject.SetActive(false);
        }
        for (int i = 0; i < nTabCounts[type]; i++)
        {
            Tabs[i].transform.parent.gameObject.SetActive(true);
            Tabs[i].text = strTabTitles[type].titles[i];
        }

        if (type == 0)
        {
            for (int i = 0; i < SheetManager.instance.BuildDB.dataArray.Length; i++)
            {
                BuildsData bData = SheetManager.instance.BuildDB.dataArray[i];
                GameObject obj = Instantiate(CenterSlotPrefabs);
                obj.transform.SetParent(CenterSlotParent);
                obj.transform.localScale = Vector3.one;
                //obj.GetComponent<CenterSlotItem>().InitSetting(bData, type);
                objList.Add(obj);
            }
        }
        else if (type == 1)
        {
            for (int i = 0; i < SheetManager.instance.MakeDB.dataArray.Length; i++)
            {
                MakeData mData = SheetManager.instance.MakeDB.dataArray[i];
                GameObject obj = Instantiate(CenterSlotPrefabs);
                obj.transform.SetParent(CenterSlotParent);
                obj.transform.localScale = Vector3.one;
                //obj.GetComponent<CenterSlotItem>().InitSetting(mData, type);
                objList.Add(obj);
            }
        }
        else if (type == 3)
        {
            for (int i = 0; i < SheetManager.instance.MolangDB.dataArray.Length; i++)
            {
                MolangData mData = SheetManager.instance.MolangDB.dataArray[i];
                GameObject obj = Instantiate(CenterSlotPrefabs);
                obj.transform.SetParent(CenterSlotParent);
                obj.transform.localScale = Vector3.one;
                //obj.GetComponent<CenterSlotItem>().InitSetting(mData, type);
                objList.Add(obj);
            }
        }

    }
    public void MakeSelected(int slotNum)
    {
        CrossHairImage.sprite = BottomObjSlots[slotNum].transform.Find("Icon").GetComponent<Image>().sprite;
        isPick = true;
        CrossHairImage.gameObject.SetActive(isPick);

        GetComponent<Image>().enabled = false;
        MakeObj.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, -200, 0);
        MakePanel.SetActive(true);
    }
    public void DropSelected()
    {
        isPick = false;
        MakeObj.SetActive(false);
        CrossHairImage.gameObject.SetActive(false);
        GameObject newObj = Instantiate(Resources.Load<GameObject>("campfire/campfire"));
        newObj.transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        newObj.transform.position = new Vector3(newObj.transform.position.x, newObj.transform.position.y, 0);
        StartCoroutine(AstarResetting());
    }
    IEnumerator AstarResetting()
    {
        yield return null;
        Astar.Scan(Astar.graphs[0]);
    }
}
