﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum BOTTOM_BUTTTON { Build, Make, MolangControl, MolangCollection, Books, Inventory, Camera }

[Serializable]
public struct TabInfo
{
    public string strId;
    public int nTabCount;
    public List<string> TabNames;
    public List<string> TabObjNames;
}
#region -- Test Information --
[Serializable]
public struct TestMakeItemInformation
{
    public string id;
    public int amount;
    public int price;
    public int nowLevel;
    public int maxLevel;
    public string name;
}
[Serializable]
public struct TestBuildUpgradeInformation
{
    public string id;
    public int amount;
    public int price;
    public int nowLevel;
    public int maxLevel;
    public string name;
}
#endregion
public class UIController : MonoBehaviour
{
    #region -- Variable --
    [Header("Object")]
    public GameObject AlphaObj;
    public GameObject CenterObj;
    public GameObject InventoryObj;
    public GameObject MakeObj;
    public GameObject ResultObj;
    public Animator CenterAnim;
    private Sprite[] FlagSprites;
    private string[] arrFlagLabelTexts;
    public GameObject BooksInfoObj;
    public GameObject BooksQuestionObj;
    public GameObject HallOfFameObj;
    public GameObject CollectionScrollViewObj;

    public UILabel FlagLabel;
    public UI2DSprite FlagImage;
    public GameObject[] ScrollViews;
    public GameObject[] TabObjs;
    public UILabel[] TabTexts;
    public TabInfo[] TabInfos;

    [Header("Scroe")]
    public Score _score;
    public UILabel MolangCountText;
    public UILabel HeartCountText;
    public UILabel StarCountText;
    public UILabel LightCountText;
    public UILabel CloverCountText;
    public UILabel HeartAmountText;
    public UILabel StarAmountText;
    private string strHeartStartText = "초당 행복 획득량X";
    private string strStarStartText = "초당 활력 획득량X";

    [Header("Tutorial")]
    public GameObject TutorialObj;
    private TutorialController tutoController;
    int nTutorialBuildAmoundCount = 150;
    public bool isTutorialBuild = false;

    [Header("Build")]
    public GameObject[] BuildStepObjs;
    public int[] BuildsPrice;
    public Material DefaultMaterial;
    public Material GreyMaterial;
    public List<Transform> SpaStepObjs = new List<Transform>();
    public List<GameObject> SpaStepEffectObjs = new List<GameObject>();
    public List<GameObject> SpaSubObjs = new List<GameObject>();
    public int nNowBuildStep = 0;
    private int nSpaLevel = 0;
    public GameObject ToolMolangGroupObj;

    [Header("Camera")]
    public Camera uiCamera;
    public CameraMove cameraMove;
    public float fCameraMoveSpeed = 4.0f;
    public float fCameraZoomSpeed = 20.0f;
    public float fCameraMinZoom = 30.0f;
    public float fCameraMaxZoom = 100.0f;
    public float fCameraZoomTime = 0.8f;

    [Header("MolangSpawn")]
    public string strSpawnMolangID = string.Empty;
    public GameObject MolangPrefabs;
    public Transform MolangGroup;
    public List<CharacterAI> MolangAiList = new List<CharacterAI>();
    public AstarPath Astar;

    [Header("Logo")]
    public LogoController logoController;

    [Header("Make & BuildUpgrade")]
    public GameObject MakeAndBuildPrefabs;

    [Header("TEST ITEM")]
    public List<TestMakeItemInformation> MAKEITEMLIST = new List<TestMakeItemInformation>();
    public List<TestBuildUpgradeInformation> BUILDUPGRADELIST = new List<TestBuildUpgradeInformation>();
    #endregion

    private void Awake()
    {
        //FlagImage = transform.Find("Center").Find("Background").Find("Flag").Find("Icon").GetComponent<UI2DSprite>();
        //FlagLabel = transform.Find("Center").Find("Background").Find("Flag").Find("Text").GetComponent<UILabel>();
        MakeAndBuildPrefabs = Resources.Load<GameObject>("Prefabs/CenterSlot");
        MolangPrefabs = Resources.Load<GameObject>("Prefabs/MolangPrefabs");

        Sprite[] LoadSprites = Resources.LoadAll<Sprite>("UI/Main/1.UI_3");
        FlagSprites = new Sprite[5];
        FlagSprites[0] = Array.Find(LoadSprites, item => item.name.Equals("Build"));
        FlagSprites[1] = Array.Find(LoadSprites, item => item.name.Equals("Make"));
        FlagSprites[2] = Array.Find(LoadSprites, item => item.name.Equals("Molang"));
        FlagSprites[3] = Array.Find(LoadSprites, item => item.name.Equals("Molang"));
        FlagSprites[4] = Array.Find(LoadSprites, item => item.name.Equals("Books"));

        arrFlagLabelTexts = new string[5] { "건설", "제작소", "몰랑이", "몰랑이", "지식" };

    }
    
    void Start()
    {
        nNowBuildStep = 0;
        for (int i = 0; i < BuildStepObjs.Length; i++)
        {
            BuildStepObjs[i].SetActive(false);
        }
        TextValueChange();
        tutoController = TutorialObj.GetComponent<TutorialController>();

        // MAKE Initialize
        // Test MakeItem
        Sprite[] _makeSprites = Resources.LoadAll<Sprite>("Icon/food");
        for (int i = 0; i < MAKEITEMLIST.Count; i++)
        {
            GameObject slotItem = Instantiate(MakeAndBuildPrefabs);
            slotItem.transform.SetParent(ScrollViews[1].transform);
            slotItem.transform.localScale = new Vector3(1, 1, 1);
            slotItem.transform.localPosition = new Vector3(0, 0, 0);
            slotItem.GetComponent<CenterSlotItem>().InitSetting(MAKEITEMLIST[i], Array.Find(_makeSprites, item => item.name.Equals(MAKEITEMLIST[i].id)), 0);
        }
        Sprite[] _buildSprites = Resources.LoadAll<Sprite>("Icon/build");
        for (int i = 0; i < BUILDUPGRADELIST.Count; i++)
        {
            GameObject slotItem = Instantiate(MakeAndBuildPrefabs);
            slotItem.transform.SetParent(ScrollViews[0].transform);
            slotItem.transform.localScale = new Vector3(1, 1, 1);
            slotItem.transform.localPosition = new Vector3(0, 0, 0);
            slotItem.GetComponent<CenterSlotItem>().InitSetting(BUILDUPGRADELIST[i], Array.Find(_buildSprites, item => item.name.Equals(BUILDUPGRADELIST[i].id)), 1);
        }

        //
        logoController.LogoEnd((bool end) =>
        {
            if(end)
            {
                if(isTutorialBuild)
                {
                    //튜토리얼 온
                    tutoController.gameObject.SetActive(true);
                }
                else
                {
                    StepChange();
                }
            }
        });
    }
    
    void Update()
    {
        if (isTutorialBuild && nTutorialBuildAmoundCount <= _score.nHeartCount && !TutorialObj.activeSelf)
        {
            isTutorialBuild = false;
            TutorialObj.SetActive(true);
        }
        //if (Input.GetMouseButtonDown(0))
        //{
        //    RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        //    if (hit)
        //    {
        //        Debug.Log(hit.collider.name);
        //    }
        //}
    }

    void LateUpdate()
    {
        //카메라 size 대응 ui 사이즈 수정
        if(uiCamera.transform.localScale.x != uiCamera.orthographicSize)
        {
            float size = uiCamera.orthographicSize;
            uiCamera.transform.localScale = new Vector3(size, size);
        }
    }

    public void ButtonClickEvent(GameObject _obj)
    {
        BOTTOM_BUTTTON paramaterEnum = (BOTTOM_BUTTTON)Enum.Parse(typeof(BOTTOM_BUTTTON), _obj.name);

        switch (paramaterEnum)
        {
            case BOTTOM_BUTTTON.Build:
                break;
            case BOTTOM_BUTTTON.Make:
                break;
            case BOTTOM_BUTTTON.MolangControl:
                break;
            case BOTTOM_BUTTTON.MolangCollection:
                break;
            case BOTTOM_BUTTTON.Books:
                break;
            case BOTTOM_BUTTTON.Inventory:
                AlphaObj.SetActive(true);
                InventoryObj.SetActive(true);
                break;
            case BOTTOM_BUTTTON.Camera:
                break;
        }

        // TAB ON OFF
        for (int i = 0; i < TabInfos.Length; i++)
        {
            if (TabInfos[i].strId.Equals(_obj.name))
            {
                for (int j = 0; j < TabObjs.Length; j++)
                {
                    if (j < TabInfos[i].nTabCount)
                    {
                        TabTexts[j].text = TabInfos[i].TabNames[j];
                        TabObjs[j].name = TabInfos[i].TabObjNames[j];
                        TabObjs[j].SetActive(true);
                    }
                    else
                    {
                        TabTexts[j].text = "";
                        TabObjs[j].name = "Tab";
                        TabObjs[j].SetActive(false);
                    }
                }
            }
        }

        // 상단 이미지 및 텍스트 변경
        if ((int)paramaterEnum < FlagSprites.Length)
        {
            FlagImage.sprite2D = FlagSprites[(int)paramaterEnum];
            FlagImage.MakePixelPerfect();
            FlagLabel.text = arrFlagLabelTexts[(int)paramaterEnum];
            for (int i = 0; i < ScrollViews.Length; i++)
            {
                if (i == (int)paramaterEnum) ScrollViews[i].SetActive(true);
                else ScrollViews[i].SetActive(false);
            }

            CenterObj.SetActive(true);
            AlphaObj.SetActive(true);
        }

    }
    public void CloseBtnEvent()
    {
        StartCoroutine(CloseCoroutine());
    }
    public void CloseBtnEventTargetObj(GameObject _obj)
    {
        _obj.SetActive(false);
    }
    IEnumerator CloseCoroutine()
    {
        CenterAnim.SetTrigger("offTrigger");
        yield return new WaitForSeconds(0.2f);
        AlphaObj.SetActive(false);
        CenterObj.SetActive(false);
        HallOfFameObj.SetActive(false);
        CollectionScrollViewObj.SetActive(true); 
        InventoryObj.SetActive(false);

    }
    public void TextValueChange()
    {
        HeartAmountText.text = strHeartStartText + _score.ChangeUnit((_score.nMolangCount * 7));
        StarAmountText.text = strStarStartText + _score.ChangeUnit(((_score.nBuildCount * 15)));
    }
    public void StepChange()
    {
        for (int i = 0; i < BuildStepObjs.Length; i++)
        {
            if (i == nNowBuildStep) BuildStepObjs[i].SetActive(true);
            else BuildStepObjs[i].SetActive(false);
        }
    }
    public void BuildStepCheck(int _nowHeartCount)
    {
        if (BuildsPrice[nNowBuildStep] <= _nowHeartCount)
        {
            BuildStepObjs[nNowBuildStep].GetComponent<UI2DSprite>().color = Color.white;
            BuildStepObjs[nNowBuildStep].transform.GetChild(0).GetComponent<UI2DSprite>().material = DefaultMaterial;
            BuildStepObjs[nNowBuildStep].transform.GetChild(1).GetChild(0).GetComponent<UI2DSprite>().material = DefaultMaterial;
        }
        else
        {
            BuildStepObjs[nNowBuildStep].GetComponent<UI2DSprite>().color = new Color32(58, 58, 58, 255);
            BuildStepObjs[nNowBuildStep].transform.GetChild(0).GetComponent<UI2DSprite>().material = GreyMaterial;
            BuildStepObjs[nNowBuildStep].transform.GetChild(1).GetChild(0).GetComponent<UI2DSprite>().material = GreyMaterial;
        }
    }
    public void BuildStepClickEvent()
    {
        MakeObj.SetActive(true);
        if (TutorialObj.activeSelf)
        {
            if (MakeObj.transform.Find("CloseBtn"))
                MakeObj.transform.Find("CloseBtn").gameObject.SetActive(false);
            tutoController.MovingArrowObj.transform.localPosition = new Vector3(0, -300);
        }
    }
    public void MakeEvent()
    {
        if (TutorialObj.activeSelf)
        {
            if (MakeObj.transform.Find("CloseBtn"))
                MakeObj.transform.Find("CloseBtn").gameObject.SetActive(true);
            tutoController.MovingArrowObj.SetActive(false);
        }

        fCameraMinZoom = 70;
        StartCoroutine(MolangSpawnEvent(new Vector3(-65500, 54400, -50), false));
        StartCoroutine(SpaBuildMake());
    }
    IEnumerator SpaBuildMake()
    {
        nNowBuildStep++;
        StepChange();
        float nowZoom = uiCamera.orthographicSize;
        float endZoom = fCameraMinZoom;
        float zoomAmount = (endZoom - nowZoom) * 0.5f * Time.fixedDeltaTime;
        while (true)
        {
            uiCamera.orthographicSize += zoomAmount;

            if (Mathf.Abs(endZoom - uiCamera.orthographicSize) <= Mathf.Abs(zoomAmount))
            {
                uiCamera.orthographicSize = endZoom;
                break;
            }
            yield return new WaitForFixedUpdate();
        }
        cameraMove.isCameraMove = false;
        if (ToolMolangGroupObj)
            ToolMolangGroupObj.SetActive(true);
        yield return new WaitForSeconds(3.0f);

        // 온천 ON
        SpaStepEffectObjs[nSpaLevel].gameObject.SetActive(true);
        yield return new WaitForSeconds(1.4f);
        SpaStepEffectObjs[nSpaLevel].gameObject.SetActive(false);
        SpaStepObjs[nSpaLevel].gameObject.SetActive(true);
        for (int i = 0; i < SpaSubObjs.Count; i++)
        {
            SpaSubObjs[i].SetActive(true);
            if (i == 0)
            {
                yield return null;
                Astar.Scan(Astar.graphs[1]);
            }

            if(SpaSubObjs[i].name.Contains("Effect"))
            {
                yield return new WaitForSeconds(UnityEngine.Random.Range(0, 1.0f));
            }
        }
        if (ToolMolangGroupObj)
            ToolMolangGroupObj.SetActive(false);
        cameraMove.isCameraMove = true;

        nowZoom = uiCamera.orthographicSize;
        endZoom = 80;
        zoomAmount = (endZoom - nowZoom) * 0.5f * Time.fixedDeltaTime;
        while (true)
        {
            uiCamera.orthographicSize += zoomAmount;

            if (Mathf.Abs(endZoom - uiCamera.orthographicSize) <= Mathf.Abs(zoomAmount))
            {
                uiCamera.orthographicSize = endZoom;
                break;
            }
            yield return new WaitForFixedUpdate();
        }
        ResultObj.SetActive(true);
        nSpaLevel++;

    }
    public void MakeEnd()
    {
        ResultObj.SetActive(false);
    }
    public void TabClickEvent(GameObject _obj)
    {
        ButtonClickEvent(_obj);
    }
    public void CloseBtnEventSetParam(GameObject _obj)
    {
        _obj.SetActive(false);
    }
    public void ObjOpenEvent(GameObject _openObj)
    {
        _openObj.SetActive(true);
        if (TutorialObj.activeSelf) tutoController.ObjOpenEvent(_openObj);
    }
    public void SaveCollectionMolang()
    {
        _score.nMolangCount++;
        TextValueChange();
        BooksInfoObj.SetActive(false);
        BooksQuestionObj.SetActive(false);
        CloseBtnEvent();
        if (TutorialObj.activeSelf)
            tutoController.SaveCollectionMolang();
        fCameraMinZoom = 30;
        StartCoroutine(MolangSpawnEvent(new Vector3(-33000, 7600, -50), true, true));
    }
    public IEnumerator MolangSpawnEvent(Vector3 _cameraEndPos, bool _isCamera = true, bool _isMolangSpawn = false)
    {
        cameraMove.isCameraMove = false;
        if (TutorialObj.activeSelf)
            tutoController.MolangSpawnEventStep(0);

        Vector3 cameraStartPos = uiCamera.transform.localPosition;
        Vector3 cameraMoveDir = (_cameraEndPos - cameraStartPos).normalized;
        while (true)
        {
            uiCamera.transform.Translate(cameraMoveDir * fCameraMoveSpeed);
            cameraMoveDir = (_cameraEndPos - uiCamera.transform.localPosition).normalized;
            float distance = Vector3.Distance(uiCamera.transform.localPosition, _cameraEndPos);
            if (distance < 1500.0f)
            {
                uiCamera.transform.localPosition = _cameraEndPos;
                break;
            }
            yield return new WaitForFixedUpdate();
        }

        if (_isCamera)
        {
            float nowZoom = uiCamera.orthographicSize;
            float endZoom = fCameraMinZoom;
            float zoomAmount = (endZoom - nowZoom) * fCameraZoomTime * Time.fixedDeltaTime;
            while (true)
            {
                uiCamera.orthographicSize += zoomAmount;

                if (Mathf.Abs(endZoom - uiCamera.orthographicSize) <= Mathf.Abs(zoomAmount))
                {
                    uiCamera.orthographicSize = endZoom;
                    break;
                }
                yield return new WaitForFixedUpdate();
            }
            if(_isMolangSpawn)
            {
                GameObject MolangObj = Instantiate(MolangPrefabs);
                CharacterAI mAi = MolangObj.GetComponent<CharacterAI>();
                mAi.strSkinName = GetSkinName(strSpawnMolangID);
                MolangAiList.Add(mAi);
                MolangObj.transform.SetParent(MolangGroup);
                MolangObj.transform.localPosition = new Vector3(-50.6f, 5.9f, 0);
                MolangObj.SetActive(true);
                mAi.mTouch.camera = uiCamera;
                mAi.mTouch.cameraMove = cameraMove;
                if (TutorialObj.activeSelf)
                    tutoController.MolangSpawnEventStep(1);

                yield return new WaitForSeconds(4.0f);
            }
        }

        cameraMove.isCameraMove = true;
    }
    public string GetSkinName(string _id)
    {
        string rValue = string.Empty;
        switch (_id)
        {
            case "basic":
                rValue = "molang_normal_basic";
                break;
            case "brown":
                rValue = "molang_normal_002brown";
                break;
            case "point":
                rValue = "molang_normal_001caramel";
                break;
            case "gray":
                rValue = "molang_normal_003grey";
                break;
            case "yellow":
                rValue = "molang_normal_004yellow";
                break;
            case "pink":
                rValue = "molang_normal_005pink";
                break;
            case "blanket":
                rValue = "molang_normal_006sleep";
                break;
            case "ricecake":
                rValue = "molang_normal_007jjinbbang";
                break;
            case "rudolf":
                rValue = "molang_normal_008rudolph";
                break;

        }
        return rValue;
    }
    public void TestBalllonSacle(TweenScale _scale)
    {
        _scale.enabled = true;
        _scale.from = _scale.to;
        if (_scale.to.x < 0)
            _scale.to = new Vector3(0.3f, 0.3f);
        else
            _scale.to = new Vector3(-0.3f, 0.3f);

        _scale.ResetToBeginning();
        _scale.PlayForward();
    }

    #region -- Temp Method --

    #endregion
}
