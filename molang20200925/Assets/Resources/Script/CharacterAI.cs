﻿using Pathfinding;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAI : MonoBehaviour
{
    [Header("AI Movement")]
    //플레이어에게 갈 수 있는지 체크
    public bool isPathPossible = false;
    //이동할 좌표를 바꿔야할 상태인지 체크
    public bool isMoveChange = true;
    // 초마다 몇번의 길 업데이트를 할것인지
    public float updateRate = 2f;
    [HideInInspector] public bool pathIsEnabled = false;
    // AI에서 다음 웨이 포인트까지 계속되는 웨이 포인트까지의 최대 거리
    public float nextWaypointDistance = 1;
    // 우리가 현재 움직이고있는 웨이 포인트
    private int currentWaypoint = 0;
    // 계산 된 경로
    public Path path;
    public bool isTargeting = false;
    private Seeker seeker;
    public Vector3 dir;
    public bool isMoveStop = false;
    public float speed = 5.0f;

    public bool isRun = false;

    [Header("Info")]
    public string strId = string.Empty;
    public string strSkinName = string.Empty;

    [Header("TEST")]
    public bool isRandomMolang = false;
    public string[] skinName;
    public bool isSpringMolang = false;

    public molangTouch mTouch;

    public AnimController anim;
    private void Awake()
    {
        SkeletonAnimation skeleton = anim.GetComponent<SkeletonAnimation>();
        //if (isRandomMolang)
        //{
        //    skeleton.initialSkinName = skinName[Random.Range(0, skinName.Length)];
        //}
        if (!strSkinName.Equals(string.Empty))
        {
            skeleton.initialSkinName = strSkinName;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        seeker = GetComponent<Seeker>();
        StartCoroutine(UpdatePath());
    }
    private void FixedUpdate()
    {
        AIMove();
    }
    void AIMove()
    {
        if (isMoveStop) return;
        if (isPathPossible && path != null)
        {
            if(!isSpringMolang)
            {
                if ((isRun ? !anim.skeletonAnimation.AnimationName.Equals("run") : !anim.skeletonAnimation.AnimationName.Equals("walk")) && !isMoveStop && !anim.isWater)
                {
                    if (isRun)
                        anim.isRun = true;
                    else
                        anim.isWalk = true;
                }
                else if (!anim.skeletonAnimation.AnimationName.Equals("swim") && !isMoveStop && anim.isWater)
                {
                    if (isRun)
                        anim.isSwim = true;
                    else
                        anim.isSwim = true;
                }
            }

            if (currentWaypoint >= path.vectorPath.Count)
            {
                if (pathIsEnabled)
                    return;
                //경로의 끝에 도착
                pathIsEnabled = true;
                isMoveChange = true;
                return;
            }
            pathIsEnabled = false;

            if (isMoveStop) return;
            // 다음 웨이 포인트 방향
            dir = (path.vectorPath[currentWaypoint] - transform.position).normalized;
            Move(dir, speed);
            float dist = Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]);
            if (dist < nextWaypointDistance)
            {
                currentWaypoint++;
                return;
            }
        }
    }
    private void Move(Vector2 axisDirection, float _speed)
    {
        if (axisDirection.x > 0) transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x) * -1, transform.localScale.y, transform.localScale.z);
        else transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
        transform.Translate(axisDirection * (isRun ? _speed * 1.5f : _speed) * Time.fixedDeltaTime);
    }
    IEnumerator UpdatePath()
    {
        int randomMotion = 0;
        randomMotion = Random.Range(0, 4);
        float motionTime = 0.0f;
        while (true)
        {
            if (isMoveChange)
            {
                if ((randomMotion != 0 && randomMotion != 1) || isSpringMolang)
                {
                    int X = Random.Range(Mathf.FloorToInt(transform.position.x) - 50, Mathf.FloorToInt(transform.position.x) + 50);
                    int Y = Random.Range(Mathf.FloorToInt(transform.position.y) - 50, Mathf.FloorToInt(transform.position.y) + 50);
                    Vector3 newPos2 = new Vector3(transform.position.x, transform.position.y, 0);
                    Vector3 newPos3 = new Vector3(X, Y, 0);
                    //현재 자신의 노드와 목표지점의 노드를 비교하여 그곳까지 갈수있는지 확인
                    GraphNode node1 = AstarPath.active.GetNearest(newPos2, NNConstraint.Default).node;
                    GraphNode node2 = AstarPath.active.GetNearest(newPos3, NNConstraint.Default).node;
                    if (node1 == null || node2 == null)
                    {
                        yield return new WaitForFixedUpdate();
                        continue;
                    }
                    //못간다
                    if (!PathUtilities.IsPathPossible(node1, node2))
                    {
                        path = null;
                        isPathPossible = false;
                    }
                    else
                        isPathPossible = true;

                    if (isPathPossible)
                    {
                        isMoveChange = false;
                        isRun = (Random.Range(0, 300) % 4 == 1 ? true : false);
                        seeker.StartPath(newPos2, newPos3, OnPathComplete);
                        while (true)
                        {
                            int t = Random.Range(0, 4);
                            if (t != randomMotion)
                            {
                                randomMotion = t;
                                break;
                            }
                        }
                    }
                }
                else if (randomMotion == 0)
                {
                    motionTime += Time.fixedDeltaTime;
                    if (!anim.skeletonAnimation.AnimationName.Equals("sitdown") && !anim.isWater)
                    {
                        isMoveStop = true;
                        anim.isSitdown = true;
                    }
                    else if (!anim.skeletonAnimation.AnimationName.Equals("water_idle") && anim.isWater)
                    {
                        isMoveStop = true;
                        anim.isWater_idle = true;
                    }
                    else
                    {
                        if (motionTime > 3.0f)
                        {
                            motionTime = 0.0f;
                            isMoveStop = false;
                            while (true)
                            {
                                int t = Random.Range(0, 4);
                                if (t != randomMotion)
                                {
                                    randomMotion = t;
                                    break;
                                }
                            }
                        }
                    }
                }
                else if (randomMotion == 1)
                {
                    motionTime += Time.fixedDeltaTime;
                    if (!anim.skeletonAnimation.AnimationName.Equals("Idle") && !isMoveStop && !anim.isWater)
                    {
                        isMoveStop = true;
                        anim.isIdle = true;
                    }
                    else if (!anim.skeletonAnimation.AnimationName.Equals("water_action_idle") && !isMoveStop && anim.isWater)
                    {
                        isMoveStop = true;
                        anim.isWater_action_idle = true;
                    }
                    else
                    {
                        if (motionTime > 2.0f)
                        {
                            motionTime = 0.0f;
                            isMoveStop = false;
                            while (true)
                            {
                                int t = Random.Range(0, 4);
                                if (t != randomMotion)
                                {
                                    randomMotion = t;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            yield return new WaitForFixedUpdate();
        }
    }
    public void OnPathComplete(Path _path)
    {
        //길에 오류가 있는지 검사
        if (!_path.error)
        {
            path = _path;
            currentWaypoint = 0;
        }
        else
            isMoveChange = true;
    }
}
