﻿using Spine;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimController : MonoBehaviour
{
    public Transform target;
    public float speed;
    public SkeletonAnimation skeletonAnimation;
    public CharacterAI charAI;
    public Rigidbody2D rb;
    public bool isTool = false;

    public bool isIdle = false;
    public bool isActionIdle = false;
    public bool isDoorIn = false;
    public bool isDoorOut = false;
    public bool isGlowControl = false;
    public bool isRun = false;
    public bool isSitdown = false;
    public bool isSwim = false;
    public bool isTool_pickup = false;
    public bool isTool_swing = false;
    public bool isWalk = false;
    public bool isWater_action_idle = false;
    public bool isWater_idle = false;
    public bool isWater_in = false;

    public bool isCampfire = false;

    [SpineEvent(dataField: "skeletonAnimation", fallbackToTextField: true)]
    EventData eventData;
    public string eventName;
    public bool isWater = false;
    public Sprite heartSprite;
    private void Awake()
    {
        skeletonAnimation = GetComponent<SkeletonAnimation>();
    }
    void Start()
    {
        eventData = skeletonAnimation.Skeleton.Data.FindEvent(eventName);
        skeletonAnimation.AnimationState.Event += HandleAnimationStateEvent;
        skeletonAnimation.AnimationState.Start += delegate (TrackEntry trackEntry) {
            StartCoroutine(AnimationEndEvent(trackEntry));
        };
        skeletonAnimation.AnimationState.End += delegate {
        };
    }
    private void HandleAnimationStateEvent(TrackEntry trackEntry, Spine.Event e)
    {
        bool eventMatch = (eventData == e.Data); // Performance recommendation: Match cached reference instead of string.
        if (eventMatch)
        {
            StartCoroutine(DoorOutEvent());
        }
    }
    IEnumerator DoorOutEvent()
    {
        float t = 0.0f;
        Vector3 startPos = transform.position;
        Vector3 endPos = transform.position + new Vector3(-3, -1.5f);
        Vector3 centerPos = transform.position + new Vector3(-1.5f, 1f);

        while (t < 1.0f)
        {
            Vector3 lerp_0 = Vector3.Lerp(startPos, centerPos, t);
            Vector3 lerp_1 = Vector3.Lerp(centerPos, endPos, t);
            Vector3 Lerp = Vector3.Lerp(lerp_0, lerp_1, t);
            transform.parent.position = Lerp;
            t += Time.fixedDeltaTime * 2;
            yield return new WaitForFixedUpdate();
        }
    }
    IEnumerator AnimationEndEvent(TrackEntry trackEntry)
    {
        yield return new WaitForSecondsRealtime(trackEntry.Animation.Duration);
        string animName = skeletonAnimation.AnimationName;
        if (animName.Equals("door_in"))
        {
            if (DoorCoroutine == null)
                DoorCoroutine = StartCoroutine(DoorInCoroutine());
        }
        else if (animName.Equals("door_out"))
        {
            if (!isDoorOutEvent)
            {
                isDoorOutEvent = true;
                isWalk = true;
                if (charAI) charAI.isMoveStop = false;
                if (charAI) charAI.isMoveChange = true;
                yield return new WaitForSeconds(5.0f);
                if (door) door.isUsing = false;
                isDoorOutEvent = false;
            }
        }
        else if (animName.Equals("water_in"))
        {
            if (!isWaterInEvent)
            {
                isWaterInEvent = true;
                isWater = true;
                if (charAI) charAI.isMoveStop = false;
                yield return new WaitForSeconds(1.0f);
                isWaterInEvent = false;
            }
        }
        else if (animName.Equals("tool_pickup"))
        {
            if (charAI) charAI.isMoveStop = true;
            isActionIdle = true;
            yield return new WaitForSeconds(5.0f);
            if (charAI) charAI.isMoveStop = false;
            if (transform.Find("Tool")) transform.Find("Tool").gameObject.SetActive(false);
            isIdle = true;
            yield return new WaitForSeconds(3.0f);
            isCampfire = false;

        }


        if (Random.Range(0, 100) % 2 == 0 && heartSprite)
        {
            GameObject heartObj = new GameObject();
            SpriteRenderer sr = heartObj.AddComponent<SpriteRenderer>();
            sr.sprite = heartSprite;
            Vector3 ppos = transform.position;
            ppos.y += 5;
            heartObj.transform.position = ppos;
            heartObj.transform.localScale = new Vector3(3, 3, 3);
            sr.sortingLayerID = skeletonAnimation.GetComponent<MeshRenderer>().sortingLayerID;
            sr.sortingOrder = 9999;
            HeartUpController controller = heartObj.AddComponent<HeartUpController>();
            controller.InitSetting();
        }
    }
    DoorEvent door;
    Coroutine DoorCoroutine = null;
    Vector3 doorEndPosition;
    bool isDoorOutEvent = false;
    bool isWaterInEvent = false;
    public void DoorInMethod(Vector3 pos, Vector3 pos2, DoorEvent _door)
    {
        charAI.isMoveStop = true;
        door = _door;
        transform.parent.position = pos;
        if (_door.isMinusScale) transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x) * -1, transform.localScale.y, transform.localScale.z);
        else transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
        doorEndPosition = pos2;
        isDoorIn = true;
    }
    IEnumerator DoorInCoroutine()
    {
        yield return new WaitForSeconds(Random.Range(3.0f, 5.0f));
        transform.parent.position = doorEndPosition;
        isDoorOut = true;
        DoorCoroutine = null;
    }
    void Update()
    {
        #region -- Animation Event InputOutput --

        if (isIdle)
        {
            isIdle = false;
            skeletonAnimation.loop = true;
            if (transform.Find("Tool") && transform.Find("Tool").gameObject.activeSelf) transform.Find("Tool").GetComponent<AnimController>().isIdle = true;
            skeletonAnimation.AnimationName = "Idle";
        }
        if (isActionIdle)
        {
            isActionIdle = false;
            skeletonAnimation.loop = false;
            if (transform.Find("Tool") && transform.Find("Tool").gameObject.activeSelf) transform.Find("Tool").GetComponent<AnimController>().isActionIdle = true;
            skeletonAnimation.AnimationName = "action_idle";
        }
        if (isDoorIn)
        {
            isDoorIn = false;
            skeletonAnimation.loop = false;
            if (transform.Find("Tool") && transform.Find("Tool").gameObject.activeSelf) transform.Find("Tool").GetComponent<AnimController>().isDoorIn = true;
            skeletonAnimation.AnimationName = "door_in";
        }
        if (isDoorOut)
        {
            isDoorOut = false;
            skeletonAnimation.loop = false;
            if (transform.Find("Tool") && transform.Find("Tool").gameObject.activeSelf) transform.Find("Tool").GetComponent<AnimController>().isDoorOut = true;
            skeletonAnimation.AnimationName = "door_out";
        }
        if (isGlowControl)
        {
            isGlowControl = false;
            skeletonAnimation.loop = false;
            if (transform.Find("Tool") && transform.Find("Tool").gameObject.activeSelf) transform.Find("Tool").GetComponent<AnimController>().isGlowControl = true;
            skeletonAnimation.AnimationName = "glow_control";
        }
        if (isRun)
        {
            isRun = false;
            skeletonAnimation.loop = true;
            if (transform.Find("Tool") && transform.Find("Tool").gameObject.activeSelf) transform.Find("Tool").GetComponent<AnimController>().isRun = true;
            skeletonAnimation.AnimationName = "run";
        }
        if (isSitdown)
        {
            isSitdown = false;
            skeletonAnimation.loop = false;
            if (transform.Find("Tool") && transform.Find("Tool").gameObject.activeSelf) transform.Find("Tool").GetComponent<AnimController>().isSitdown = true;
            skeletonAnimation.AnimationName = "sitdown";
        }
        if (isSwim)
        {
            isSwim = false;
            skeletonAnimation.loop = true;
            if (transform.Find("Tool") && transform.Find("Tool").gameObject.activeSelf) transform.Find("Tool").GetComponent<AnimController>().isSwim = true;
            skeletonAnimation.AnimationName = "swim";
        }
        if (isTool_pickup)
        {
            isTool_pickup = false;
            skeletonAnimation.loop = false;
            if (transform.Find("Tool")) transform.Find("Tool").gameObject.SetActive(true);
            if (transform.Find("Tool")) transform.Find("Tool").GetComponent<AnimController>().isTool_pickup = true;
            skeletonAnimation.AnimationName = "tool_pickup";
        }
        if (isTool_swing)
        {
            isTool_swing = false;
            skeletonAnimation.loop = true;
            if (transform.Find("Tool")) transform.Find("Tool").gameObject.SetActive(true);
            if (transform.Find("Tool")) transform.Find("Tool").GetComponent<AnimController>().isTool_swing = true;
            skeletonAnimation.AnimationName = "tool_swing";
        }
        if (isWalk)
        {
            isWalk = false;
            skeletonAnimation.loop = true;
            if (transform.Find("Tool") && transform.Find("Tool").gameObject.activeSelf) transform.Find("Tool").GetComponent<AnimController>().isWalk = true;
            skeletonAnimation.AnimationName = "walk";
        }
        if (isWater_action_idle)
        {
            isWater_action_idle = false;
            skeletonAnimation.loop = false;
            if (transform.Find("Tool") && transform.Find("Tool").gameObject.activeSelf) transform.Find("Tool").GetComponent<AnimController>().isWater_action_idle = true;
            skeletonAnimation.AnimationName = "water_action_idle";
        }
        if (isWater_idle)
        {
            isWater_idle = false;
            skeletonAnimation.loop = true;
            if (transform.Find("Tool") && transform.Find("Tool").gameObject.activeSelf) transform.Find("Tool").GetComponent<AnimController>().isWater_idle = true;
            skeletonAnimation.AnimationName = "water_idle";
        }
        if (isWater_in)
        {
            isWater_in = false;
            skeletonAnimation.loop = false;
            if (transform.Find("Tool") && transform.Find("Tool").gameObject.activeSelf) transform.Find("Tool").GetComponent<AnimController>().isWater_in = true;
            skeletonAnimation.AnimationName = "water_in";
        }
        #endregion
        //int t = Mathf.FloorToInt(transform.position.y * 100);
        //t = 9999 - t;
        //if (isTool && transform.Find("Tool")) t += 1;
        //if (isTool && !transform.Find("Tool")) t += 2;
        //skeletonAnimation.GetComponent<MeshRenderer>().sortingOrder = t;
        //
        //if (Mathf.FloorToInt(transform.position.y) > 41)
        //{
        //    skeletonAnimation.GetComponent<MeshRenderer>().sortingLayerName = "ground0";
        //}
        //else if (Mathf.FloorToInt(transform.position.y) > 12)
        //{
        //    skeletonAnimation.GetComponent<MeshRenderer>().sortingLayerName = "ground2";
        //}
        //else
        //{
        //    skeletonAnimation.GetComponent<MeshRenderer>().sortingLayerName = "ground3";
        //}
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer.Equals(4))
        {
            charAI.isMoveStop = true;
            isWater_in = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer.Equals(4))
        {
            isWater = false;
            isIdle = true;
        }
    }
}
