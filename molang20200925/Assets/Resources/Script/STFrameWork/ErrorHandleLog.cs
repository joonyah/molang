﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class ErrorHandleLog : MonoBehaviour
{
    //log
    private Action<string> callbackLog;

    public string strLog;

    public UILabel labelLog;

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    //error 핸들러
    void OnEnable()
    {
        Application.logMessageReceived += HandleLog;
    }
    void OnDisable()
    {
        Application.logMessageReceived -= HandleLog;
    }

    //db에 기록할 오류 내용
    private void HandleLog(string logString, string stackTrace, LogType type)
    {
        if (type == LogType.Error)
        {
            callbackLog(logString);
            strLog += logString + "\n";
            if (labelLog != null)
                labelLog.text = strLog;
        }
    }

    public void GetLog(Action<string> _log)
    {
        callbackLog = _log;
    }
}
