﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;
using System;

public class ServerTest : MonoBehaviour
{
    public Text text;

    void Start()
    {
        GetDB();
    }

    void GetDB()
    {
        //action 코루틴
        string data = "id=" + "TESTTEST";
        StartCoroutine(httpRequest.RequestGet("User/Get?", data, (response, value) =>
        {
            //success
            if (response == "success")
            {
                var Json = JSON.Parse(value);
                if (Json[0] != null)
                {
                    Debug.Log("Json Data[ id ] : " + Json[1]["id"].Value);
                }                
            }
            Debug.Log("response : " + response + " / " + " value : " + value);
        }));
    }

    void UpdateDB()
    {
        //action 코루틴
        string data = "id=" + "TTT" + "&name=" + "유니티에서 바꿔요" + "&star=" + "35";
        StartCoroutine(httpRequest.RequestGet("User/Update?", data, (response, value) =>
        {
            //success
            if (response == "success")
            {

            }
            Debug.Log("response : " + response + " / " + " value : " + value);
        }));
    }


    void LoginDB()
    {
        //action 코루틴
        string data = "id=" + "TESTTEST" + "&name=" + "유니티에서 보내요";
        StartCoroutine(httpRequest.RequestGet("User/Login?", data, (response, value) =>
        {
            //success
            if (response == "success")
            {
                var Json = JSON.Parse(value);
                if (Json == null)
                    return;

                Debug.Log("Json Data[ id ] : " + Json[1]["id"].Value);
            }
            Debug.Log("response : " + response + " / " + " value : " + value);
        }));
    }
}
