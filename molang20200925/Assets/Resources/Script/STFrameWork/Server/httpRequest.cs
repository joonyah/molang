﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Encryption;
using UnityEngine.UIElements;

public class httpRequest : MonoBehaviour
{
    //후에 Define 으로 선언 할 내용
    //error code 01 => 유저 정보가 없습니다.

    //기본 url 정보
    //private static string baseurl = "http://192.168.1.84:8000/molang/";
    private static string baseurl = "http://reozma.ipdisk.co.kr:8000/molang/";

    /*
    string data = "Id=" + Id + "&Age=" + Age.ToString();
    StartCoroutine(httpRequest.RequestGet("Test/GetDB?", data, (response) =>
    {
        //success
        if (response == "success")
        {
        }
    }));
    */

    public static httpRequest Instance
    {
        get
        {
            if (Instance == null)
            {
                Instance = new httpRequest();
            }
            return Instance;
        }
        private set
        {
            Instance = value;
        }
    }

    void Start()
    {
        DontDestroyOnLoad(this);
    }

    public static IEnumerator RequestGet(string _url, string _data, Action<string, string> _result)
    {
        //aes 128 암호화 사용
        string AESData = AESCrypto.AESEncrypt128(_data);
        Debug.Log("full url: " + baseurl + _url + AESData);

        //Get Send
        //타임아웃 기능 
        using (UnityWebRequest www = UnityWebRequest.Get(baseurl + _url + AESData))
        {
            www.timeout = 5;
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                _result("error", www.error);
            }
            else
            {
                _result("success", www.downloadHandler.text);
            }
        }
    }

    public static IEnumerator RequestGetPost<T>(string _url, Dictionary<T, T> _data, Action<string> _result)
    {
        WWWForm form = new WWWForm();

        //Post 형식 제작
        //form.AddField(_key, _value);

        //PostSend
        using (UnityWebRequest www = UnityWebRequest.Post(baseurl + _url, form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                _result("error: " + www.error);
            }
            else
            {
                _result("success: " + www.downloadHandler.text);
            }
        }
    }
}
