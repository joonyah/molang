﻿using System.Text;
using System;
using System.IO;
using System.Security.Cryptography;

namespace Encryption
{
    //base64
    public class EncodingHelper
    {
        //암호화
        public static string Base64Encoding(string EncodingText, System.Text.Encoding oEncoding = null)
        {
            if (oEncoding == null)
                oEncoding = Encoding.UTF8;

            byte[] arr = oEncoding.GetBytes(EncodingText);
            return System.Convert.ToBase64String(arr);
        }

        //복호화
        public static string Base64Decoding(string DecodingText, System.Text.Encoding oEncoding = null)
        {
            if (oEncoding == null)
                oEncoding = Encoding.UTF8;

            byte[] arr = System.Convert.FromBase64String(DecodingText);
            return oEncoding.GetString(arr);
        }
    }

    public class AESCrypto
    {
        // 암호
        private static readonly string strPASSWORD = "CTB5oY5lVsbmqDWDHhUHpZLTl";

        // 인증키
        private static readonly string strKEY = strPASSWORD.Substring(0, 128 / 8);

        // 암호화
        public static string AESEncrypt128(string plain)
        {
            byte[] plainBytes = Encoding.UTF8.GetBytes(plain);

            RijndaelManaged myRijndael = new RijndaelManaged();
            myRijndael.Mode = CipherMode.CBC;
            myRijndael.Padding = PaddingMode.PKCS7;
            myRijndael.KeySize = 128;

            MemoryStream memoryStream = new MemoryStream();

            ICryptoTransform encryptor = myRijndael.CreateEncryptor(Encoding.UTF8.GetBytes(strKEY), Encoding.UTF8.GetBytes(strKEY));

            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainBytes, 0, plainBytes.Length);
            cryptoStream.FlushFinalBlock();

            byte[] encryptBytes = memoryStream.ToArray();

            string encryptString = Convert.ToBase64String(encryptBytes);

            cryptoStream.Close();
            memoryStream.Close();

            return encryptString;
        }

        // 복호화
        public static string AESDecrypt128(string encrypt)
        {
            byte[] encryptBytes = Convert.FromBase64String(encrypt);

            RijndaelManaged myRijndael = new RijndaelManaged();
            myRijndael.Mode = CipherMode.CBC;
            myRijndael.Padding = PaddingMode.PKCS7;
            myRijndael.KeySize = 128;

            MemoryStream memoryStream = new MemoryStream(encryptBytes);

            ICryptoTransform decryptor = myRijndael.CreateDecryptor(Encoding.UTF8.GetBytes(strKEY), Encoding.UTF8.GetBytes(strKEY));

            CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);

            byte[] plainBytes = new byte[encryptBytes.Length];

            int plainCount = cryptoStream.Read(plainBytes, 0, plainBytes.Length);

            string plainString = Encoding.UTF8.GetString(plainBytes, 0, plainCount);

            cryptoStream.Close();
            memoryStream.Close();

            return plainString;
        }
    }
}
