﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WorldManager : MonoBehaviour
{
    public static WorldManager instance;

    public int nowStep = -1;
    public GameObject ResultObj;

    public Transform[] StepPositions;
    public GameObject[] StepObjs;

    public GameObject MakeObj;

    public GameObject[] StepOpenObj_0;
    public GameObject[] StepOpenObj_0_Molang;

    public RectTransform DownTransform;
    public RectTransform UpTransform;
    public GameObject[] OpenObjs;

    public GameObject BirthMolangObj;
    public GameObject[] BirthMolangObjs;

    public CenterFrameManager centerManager;
    public float fCameraSpeed = 80.0f;
    public float fCameraSpeed2 = 35.0f;

    public GameObject ResultPanelObj;
    public GameObject[] SpaLevelUpObjs;

    public GameObject PostProsessingObj;

    public Vector3[] BuildPositions;
    public bool isCameraMode = false;

    GameObject LockOnImage;

    private void Awake()
    {
        if (instance == null) instance = this;
        if (instance != this) Destroy(this.gameObject);
    }
    void Start()
    {

        LockOnImage = GetSceneObj("ScriptScene", new string[2] { "Canvas", "CameraModeLockOnImage" });
        //for (int i = 0; i < SheetManager.instance.BuildDB.dataArray.Length; i++)
        //{
        //    int step = SheetManager.instance.BuildDB.dataArray[i].STEP;
        //    if (nowStep < step)
        //    {
        //        nowStep = step;
        //        int loadData = SaveAndLoadManager.instance.LoadUnlock(SheetManager.instance.BuildDB.dataArray[i].ID);
        //        if (loadData == -1 || loadData == 0) break;
        //    }
        //}
        nowStep = 1;
        for (int i = 0; i < StepObjs.Length; i++) StepObjs[i].SetActive(false);
        if (nowStep >= 0)
        {
            StepObjs[nowStep].SetActive(true);
            if (nowStep > 0)
            {
                for (int i = 0; i < StepOpenObj_0.Length; i++) StepOpenObj_0[i].SetActive(true);
            }
        }
    }
    void Update()
    {
        if (nowStep != -1)
            StepObjs[nowStep].transform.position = Camera.main.WorldToScreenPoint(StepPositions[nowStep].position);

        if(Input.touchCount > 0)
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position), Vector2.zero, 0, 1 << 8);
            if (hit)
            {
                if (isCameraMode)
                {
                    TargetCamera.instance.target = hit.collider.gameObject.transform;
                    LockOnImage.GetComponent<Animator>().Rebind();
                }
            }
        }
        if (isCameraMode)
        {
            if (TargetCamera.instance.target != null)
            {
                if (!LockOnImage.activeSelf) LockOnImage.SetActive(true);
                Vector3 pos = TargetCamera.instance.target.position;
                pos.y += 2.0f;
                LockOnImage.transform.position = Camera.main.WorldToScreenPoint(pos);
            }
            else
            {
                if (LockOnImage.activeSelf) LockOnImage.SetActive(false);
            }
        }
        else
        {
            if (LockOnImage.activeSelf) LockOnImage.SetActive(false);
        }
    }
    public void ShowStepMake(string _id)
    {
        MakeObj.SetActive(true);

        BuildsData bData = System.Array.Find(SheetManager.instance.BuildDB.dataArray, item => item.ID.Equals(_id));
        //MakeObj.transform.GetChild(0).GetComponent<MakeController>().InitSetting(bData);
    }
    public void StepMake(string _id)
    {
        MakeObj.SetActive(false);
        if (_id.Equals("spa"))
        {
            StartCoroutine(BrithCoroutine(BuildPositions[0], 1, 0));
        }
    }
    public IEnumerator Timer(float maxTime, GameObject TimerOffObj, GameObject TimerOnObj, Image TimerObj, TextMeshProUGUI TimerText, int MakeType, object MakeData)
    {
        TimerOffObj.SetActive(false);
        TimerOnObj.SetActive(true);
        float time = 0.0f;
        while (true)
        {
            TimerObj.fillAmount = time / maxTime;
            time += Time.fixedDeltaTime;
            TimerText.text = "남은시간 : " + Mathf.CeilToInt(maxTime - time).ToString("F0") + "초";
            if (time >= maxTime) break;
            yield return new WaitForFixedUpdate();
        }
        TimerOffObj.SetActive(true);
        TimerOnObj.SetActive(false);
        if (MakeType == 0)
        {
            int nowLevel = SaveAndLoadManager.instance.LoadUnlock((MakeData as BuildsData).ID);
            SpaLevelUpObjs[nowLevel].SetActive(true);
            SaveAndLoadManager.instance.SaveUnlock(new UnLockDatas { id = "spa", level = nowLevel + 1 }, nowLevel + 1);
        }
        else
        {
            SaveAndLoadManager.instance.SaveItem(
                (MakeType == 0 ? (MakeData as BuildsData).ID :
                (MakeType == 1 ? (MakeData as MakeData).ID :
                (MakeType == 3 ? (MakeData as MolangData).ID : (MakeData as MakeData).ID))), 1, true, MakeType);
        }
    }
    public void OpenUIEventFunction()
    {
        StartCoroutine(OpenUIEvent(false));
    }
    IEnumerator OpenUIEvent(bool _isCameraMode)
    {
        float y1 = 72.0f;
        float y2 = -120.0f;
        DownTransform.anchoredPosition3D = new Vector3(0, y1, 0);
        UpTransform.anchoredPosition3D = new Vector3(0, y2, 0);

        if (_isCameraMode)
        {
            GameObject obj = GetSceneObj("ScriptScene", new string[2] { "Canvas", "CameraMode" });
            if (obj)
            {
                StartCoroutine(CameraModeOff(obj));

            }
        }

        while (true)
        {
            y1 -= Time.fixedDeltaTime * (72 / 1.5f);
            y2 += Time.fixedDeltaTime * (120 / 1.5f);
            DownTransform.anchoredPosition3D = new Vector3(0, y1, 0);
            UpTransform.anchoredPosition3D = new Vector3(0, y2, 0);
            if (y1 <= 0) break;
            yield return new WaitForFixedUpdate();
        }
        DownTransform.anchoredPosition3D = new Vector3(0, 0, 0);
        UpTransform.anchoredPosition3D = new Vector3(0, 0, 0);

        for (int i = 0; i < OpenObjs.Length; i++) OpenObjs[i].SetActive(true);
    }
    public IEnumerator CloseUIEvent(bool _isCameraMode)
    {
        isCameraMode = _isCameraMode;
        float y1 = 0;
        float y2 = 0;
        DownTransform.anchoredPosition3D = new Vector3(0, y1, 0);
        UpTransform.anchoredPosition3D = new Vector3(0, y2, 0);

        if (isCameraMode)
        {
            GameObject obj = GetSceneObj("ScriptScene", new string[2] { "Canvas", "CameraMode" });
            if (obj)
            {
                StartCoroutine(CameraModeOn(obj));

            }
        }
        while (true)
        {
            y1 += Time.fixedDeltaTime * (72 / 1);
            y2 -= Time.fixedDeltaTime * (120 / 1);
            DownTransform.anchoredPosition3D = new Vector3(0, y1, 0);
            UpTransform.anchoredPosition3D = new Vector3(0, y2, 0);
            if (y1 >= 72) break;
            yield return new WaitForFixedUpdate();
        }
        DownTransform.anchoredPosition3D = new Vector3(0, 72, 0);
        UpTransform.anchoredPosition3D = new Vector3(0, -120, 0);

        for (int i = 0; i < OpenObjs.Length; i++) OpenObjs[i].SetActive(false);

        if (isCameraMode)
        {
            if (mCount > 0) TargetCamera.instance.target = BirthMolangObjs[Random.Range(0, mCount)].transform;
        }
    }

    IEnumerator CameraModeOn(GameObject obj)
    {
        float y = -192.0f;
        obj.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, y, 0);
        while(true)
        {
            y += Time.fixedDeltaTime * 250;
            obj.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, y, 0);
            if (y >= 0) break;
            yield return new WaitForFixedUpdate();
        }
        obj.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, 0, 0);
    }
    IEnumerator CameraModeOff(GameObject obj)
    {
        float y = 0;
        obj.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, y, 0);
        while (true)
        {
            y -= Time.fixedDeltaTime * 250;
            obj.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, y, 0);
            if (y <= -192) break;
            yield return new WaitForFixedUpdate();
        }
        obj.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, -192, 0);
    }

    public void BirthMolang()
    {
        centerManager.CloseFrame();
        StartCoroutine(BrithCoroutine(new Vector3(-51, 8, -171), 0, 0));
    }
    int mCount = 0;
    public IEnumerator BrithCoroutine(Vector3 endPos, int type, int _level)
    {
        #region -- Camera Move --
        Vector3 startPos = Camera.main.transform.position;
        Vector3 dir = endPos - startPos;
        dir.Normalize();
        while(true)
        {
            Camera.main.transform.Translate(dir * fCameraSpeed * Time.fixedDeltaTime);
            if (Vector3.Distance(Camera.main.transform.position, endPos) < 1.0f)
                break;
            yield return new WaitForFixedUpdate();
        }
        Camera.main.transform.position = endPos;
        float nowSize = Camera.main.orthographicSize;
        float endSize = 20;
        if (type == 0)
            endSize = 20;
        else
            endSize = 30;
        while (true)
        {
            nowSize -= Time.fixedDeltaTime * fCameraSpeed2 * 1.2f;
            Camera.main.orthographicSize = nowSize;
            if (nowSize <= endSize) break;
            yield return new WaitForFixedUpdate();
        }
        Camera.main.orthographicSize = endSize;
        yield return new WaitForSeconds(0.5f);
#endregion

        if (type == 0 /* 몰랑이 */)
        {
            BirthMolangObj.SetActive(true);
            yield return new WaitForSeconds(3.666f);
            BirthMolangObj.SetActive(false);
            BirthMolangObjs[mCount].SetActive(true);
            mCount++;
        }
        else if(type == 1 /* 온천 */)
        {
            if (_level == 0) 
            {
                StepObjs[nowStep++].SetActive(true);
                StepObjs[nowStep].SetActive(false);
            }
            for (int i = 0; i < StepOpenObj_0_Molang.Length; i++)
            {
                StepOpenObj_0_Molang[i].SetActive(true);
                StepOpenObj_0_Molang[i].GetComponent<AnimController>().isTool_swing = true;
                StepOpenObj_0_Molang[i].GetComponent<AnimController>().skeletonAnimation.timeScale = Random.Range(0.8f, 1.3f);
            }
            yield return new WaitForSeconds(3.0f);
        }

        if (type == 0) endSize = 40;
        else endSize = 50;

        while (true)
        {
            nowSize += Time.fixedDeltaTime * fCameraSpeed2;
            Camera.main.orthographicSize = nowSize;
            if (nowSize >= endSize) break;
            yield return new WaitForFixedUpdate();
        }
        Camera.main.orthographicSize = endSize;

        if (type == 0)
        {
            ResultPanelObj.transform.Find("Title").GetComponent<TextMeshProUGUI>().text = "창조완료";
            ResultPanelObj.transform.Find("Image").Find("Image").GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/molang/basic");
           // ScoreManager.instance.molangCount++;
        }
        else if (type == 1)
        {
            for (int i = 0; i < StepOpenObj_0_Molang.Length; i++) StepOpenObj_0_Molang[i].SetActive(false);
            if (_level == 0)
            {
                ResultPanelObj.transform.Find("Title").GetComponent<TextMeshProUGUI>().text = "제작완료";
             //   ScoreManager.instance.buildCount++;
            }
            else
                ResultPanelObj.transform.Find("Title").GetComponent<TextMeshProUGUI>().text = (_level + 1) + "단계 증축 완료";

            ResultPanelObj.transform.Find("Image").Find("Image").GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/build/spa");
            ResultObj.SetActive(true);
            SpaLevelUpObjs[_level].SetActive(true);
            SaveAndLoadManager.instance.SaveUnlock(new UnLockDatas { id = "spa", level = _level + 1 }, _level + 1);
        }
        ResultPanelObj.SetActive(true);
    }

    public GameObject GetSceneObj(string SceneName, string[] ObjName)
    {
        GameObject[] objs = SceneManager.GetSceneByName(SceneName).GetRootGameObjects();
        int maxCount = ObjName.Length;
        int nowCount = 0;
        GameObject nowObj = null;
        for (int i = 0; i < objs.Length; i++)
        {
            if (objs[i].name.Equals(ObjName[nowCount]))
            {
                nowObj = objs[i];
                nowCount++;
                while (true)
                {
                    if (maxCount == nowCount) break;
                    nowObj = nowObj.transform.Find(ObjName[nowCount]).gameObject;
                    nowCount++;
                }
            }
        }
        return nowObj;
    }
    public void CloseCameraMode()
    {
        isCameraMode = false;
        PostProsessingObj.SetActive(false);
        StartCoroutine(OpenUIEvent(true));
        TargetCamera.instance.target = null;
    }
    public void CameraModeEffectOnOff(bool _isActive)
    {
        PostProsessingObj.SetActive(_isActive);
    }
}
