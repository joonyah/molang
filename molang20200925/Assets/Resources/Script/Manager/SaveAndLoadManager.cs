﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using TMPro;
using UnityEngine;

[Serializable]
public struct UnLockDatas
{
    public string id;
    public int level;
}
[Serializable]
public struct SaveItem
{
    public string id;
    public int type;
    public int count;
}
public class SaveAndLoadManager : MonoBehaviour
{
    string m_strPath = "SaveData/";
    string dPath;
    public static SaveAndLoadManager instance;

    private void Awake()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            dPath = Application.persistentDataPath;
        }
        else
        {
            dPath = Application.dataPath;
            int t = dPath.LastIndexOf('/');
            dPath = dPath.Substring(0, t);
        }

        if (instance == null) instance = this;
        if (instance != this) Destroy(this.gameObject);

        DirectoryInfo di = new DirectoryInfo(dPath + "/" + m_strPath);
        if (di.Exists == false)
        {
            di.Create();
        }
    }
    public void SaveUnlock(UnLockDatas _save, int level)
    {
        var b = new BinaryFormatter();
        List<UnLockDatas> saveList = new List<UnLockDatas>();
        if (File.Exists(dPath + "/" + m_strPath + "Unlock.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "Unlock.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            UnLockDatas[] saveDataLoad = ByteToObject(saveDataTemp) as UnLockDatas[];
            saveList = saveDataLoad.ToList();
            bool isChange = false;
            for (int i = 0; i < saveList.Count; i++)
            {
                if (saveList[i].id.Equals(_save.id))
                {
                    UnLockDatas changeData = saveList[i];
                    changeData.level = level;
                    saveList[i] = changeData;
                    isChange = true;
                    break;
                }
            }
            if (!isChange) saveList.Add(_save);
            stream.Close();
            byte[] saveData = ObjectToByte(saveList.ToArray());
            var f = File.Create(dPath + "/" + m_strPath + "Unlock.Data");
            b.Serialize(f, saveData);
            f.Close();
        }
        else
        {
            var f = File.Create(dPath + "/" + m_strPath + "Unlock.Data");
            saveList.Add(_save);
            byte[] saveData = ObjectToByte(saveList.ToArray());
            b.Serialize(f, saveData);
            f.Close();
        }
    }
    public int LoadUnlock(string _id)
    {

        var b = new BinaryFormatter();
        if (File.Exists(dPath + "/" + m_strPath + "Unlock.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "Unlock.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            UnLockDatas[] saveDataLoad = ByteToObject(saveDataTemp) as UnLockDatas[];
            stream.Close();
            for (int i = 0; i < saveDataLoad.Length; i++)
            {
                if (saveDataLoad[i].id.Equals(_id)) return saveDataLoad[i].level;
            }
        }
        return -1;
    }
    public void SaveItem(string _id, int _count, bool isNoSum, int _type)
    {
        var b = new BinaryFormatter();
        List<SaveItem> saveList = new List<SaveItem>();
        if (File.Exists(dPath + "/" + m_strPath + "Inventory.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "Inventory.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            SaveItem[] saveDataLoad = ByteToObject(saveDataTemp) as SaveItem[];
            saveList = saveDataLoad.ToList();
            bool isChange = false;
            for (int i = 0; i < saveList.Count; i++)
            {
                if (saveList[i].id.Equals(_id))
                {
                    SaveItem changeData = saveList[i];
                    if (isNoSum)
                        changeData.count = _count;
                    else
                        changeData.count += _count;
                    saveList[i] = changeData;
                    isChange = true;
                    break;
                }
            }
            if (!isChange) saveList.Add(new SaveItem { id = _id, count = _count, type = _type });
            stream.Close();
            byte[] saveData = ObjectToByte(saveList.ToArray());
            var f = File.Create(dPath + "/" + m_strPath + "Inventory.Data");
            b.Serialize(f, saveData);
            f.Close();
        }
        else
        {
            var f = File.Create(dPath + "/" + m_strPath + "Inventory.Data");
            saveList.Add(new SaveItem { id = _id, count = _count, type = _type });
            byte[] saveData = ObjectToByte(saveList.ToArray());
            b.Serialize(f, saveData);
            f.Close();
        }
    }
    public SaveItem LoadItem(string _id)
    {

        var b = new BinaryFormatter();
        if (File.Exists(dPath + "/" + m_strPath + "Inventory.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "Inventory.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            SaveItem[] saveDataLoad = ByteToObject(saveDataTemp) as SaveItem[];
            stream.Close();
            for (int i = 0; i < saveDataLoad.Length; i++)
            {
                if (saveDataLoad[i].id.Equals(_id)) return saveDataLoad[i];
            }
        }
        return new SaveItem { id = _id, count = 0 };
    }
    public SaveItem[] LoadItemAll()
    {

        var b = new BinaryFormatter();
        if (File.Exists(dPath + "/" + m_strPath + "Inventory.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "Inventory.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            SaveItem[] saveDataLoad = ByteToObject(saveDataTemp) as SaveItem[];
            stream.Close();
            return saveDataLoad;
        }
        return null;
    }
    #region -- Byte to Obj And Obj to Byte --
    public byte[] ObjectToByte(object obj)
    {
        try
        {
            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(stream, obj);
                return stream.ToArray();
            }
        }
        catch (Exception e)
        {
            Debug.LogError("아니 왜 직렬화가 !? O->B  = " + e.Message);
        }
        return null;

    }
    public object ByteToObject(byte[] buffer)
    {
        try
        {
            using (MemoryStream stream = new MemoryStream(buffer))
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                stream.Position = 0;
                return binaryFormatter.Deserialize(stream);
            }
        }
        catch (Exception e)
        {
            Debug.LogError("아니 왜 직렬화가 !? B->O = " + e.Message);
        }
        return null;
    }
    #endregion
}
