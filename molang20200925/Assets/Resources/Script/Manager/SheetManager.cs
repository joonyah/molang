﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheetManager : MonoBehaviour
{
    public static SheetManager instance;


    public Builds BuildDB;
    public Make MakeDB;
    public Molang MolangDB;

    private void Awake()
    {
        if (instance == null) instance = this;
        if (instance != this) Destroy(this.gameObject);
    }
}
