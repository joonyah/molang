﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleController : MonoBehaviour
{
    //별빛 파티클
    private List<ParticleSystem> starParticle = new List<ParticleSystem>();

    //별빛 파티클 오브젝트 부모
    public GameObject goStar;

    //라이트 컨트롤러
    public LightColorController lightController;

    void Start()
    {
        ParticleSystem[] particle = goStar.GetComponentsInChildren<ParticleSystem>();
        for(int i = 0; i < particle.Length; i++)
        {
            starParticle.Add(particle[i]);
        }
    }

    void FixSize()
    {
        //for (int i = 0; i < starParticle.Count; i++)
        //{
        //    starParticle[i].
        //}
    }
}
