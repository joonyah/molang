﻿using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class molangTouch : MonoBehaviour
{
    [SerializeField]
    private bool isTouch;

    private Transform transMoveAI;
    private Transform transMolang;
    private SkeletonAnimation skelMolang;

    CharacterAI mAI;

    [SerializeField]
    private Transform transHellowPet;
    [SerializeField]
    private SkeletonAnimation skelHellowPet;
    [SerializeField]
    private Transform transTail;

    //
    private Vector3 worldPosition;
    private Vector3 hitOffset;
    private Vector3 centerOffset;

    //메인카메라
    public Camera camera;
    public CameraMove cameraMove;


    void Start()
    {
        //
        transMolang = this.transform.parent;
        skelMolang = transMolang.gameObject.GetComponent<SkeletonAnimation>();

        //
        transHellowPet = this.transform.GetChild(0);
        skelHellowPet = transHellowPet.gameObject.GetComponent<SkeletonAnimation>();

        transHellowPet.gameObject.SetActive(false);

        //상위 오브젝트
        transMoveAI = transMolang.parent;
        mAI = transMoveAI.GetComponent<CharacterAI>();
    }

    void Update()
    {
        if (Input.GetMouseButton(0) && isTouch)
        {
            Vector3 vPosition = camera.ScreenToWorldPoint(Input.mousePosition);
            transMoveAI.localPosition = Vector3.Lerp(transMoveAI.localPosition, (vPosition + hitOffset) + new Vector3(0f, -1f), Time.deltaTime * 6f);
            transTail.localPosition = Vector3.Lerp(transTail.localPosition, new Vector3(0f, 15f), Time.deltaTime * 6f);
        }
    }

    public void OnPressMolang()
    {
        isTouch = true;

        transHellowPet.gameObject.SetActive(true);
        cameraMove.isCameraMove = false;
        mAI.isMoveStop = true;

        //스크린 월드 좌표 가져오기
        worldPosition = camera.ScreenToWorldPoint(Input.mousePosition);
        hitOffset = transMoveAI.localPosition - worldPosition;

        skelMolang.state.SetAnimation(0, "hellopet_pickup", true);
        //몰랑이 종류에 따라 변경
        skelHellowPet.state.SetAnimation(0, "hellopet_pickup", true);

        transTail.localPosition = new Vector3(0f, 15f);
        Debug.LogError(camera.ScreenToWorldPoint(Input.mousePosition - transMoveAI.localPosition));

        //centor -82, -39
    }

    public void OnReleaseMolang()
    {
        isTouch = false;

        transHellowPet.gameObject.SetActive(false);
        cameraMove.isCameraMove = true;
        mAI.isMoveChange = true;

        skelMolang.state.SetAnimation(0, "Idle", true);
        skelHellowPet.state.SetAnimation(0, "Idle", true);

        transTail.localPosition = new Vector3(-1f, 4.3f);
    }

    public void OnDragStart()
    {
        //Debug.LogError(" OnDragStart ");
    }

    public void OnDragOver()
    {
        //Debug.LogError(" OnDragOver ");
    }

    public void OnDragOut()
    {
        //Debug.LogError(" OnDragOut ");
    }
}
