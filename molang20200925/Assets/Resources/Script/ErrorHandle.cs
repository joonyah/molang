﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class ErrorHandle : MonoBehaviour
{
    //log
    private static Action<string> callbackLog;

    //error 핸들러
    void OnEnable()
    {
        Application.logMessageReceived += HandleLog;
    }
    void OnDisable()
    {
        Application.logMessageReceived -= HandleLog;
    }

    //db에 기록할 오류 내용
    void HandleLog(string logString, string stackTrace, LogType type)
    {
        if (type == LogType.Error)
        {
            callbackLog(logString);
            //Debug.Log("Handle Error Log : " + logString);
        }
    }

    public static void GetLog(Action<string> _log)
    {
        callbackLog = _log;
    }
}
