﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum CollisionType { BOX, CIRCLE, CAPSULE};
public class DoorEvent : MonoBehaviour
{
    [SerializeField] private Vector3 offSet;
    [SerializeField] private Vector3 vBoxSize;
    [SerializeField] private float fCircleRadius;
    [SerializeField] private CollisionType cType;
    [SerializeField] private CapsuleDirection2D capsuleDirection;
    [SerializeField] private LayerMask mask;
    [SerializeField] private int rootMag = 0;
    [SerializeField] private bool isRootMinus;
    [SerializeField] private GameObject DoorInObj;
    [SerializeField] private GameObject DoorOutObj;
    public bool isMinusScale;
    public bool isUsing = false;


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        if (cType == CollisionType.BOX) Gizmos.DrawWireCube(transform.position + ((offSet * (isRootMinus ? -1 : 1)) * rootMag), (vBoxSize * rootMag));
        else if (cType == CollisionType.CIRCLE) Gizmos.DrawWireSphere(transform.position + ((offSet * (isRootMinus ? -1 : 1)) * rootMag), (fCircleRadius * rootMag));
    }

    private void Update()
    {
        RaycastHit2D hit;

        if (cType == CollisionType.BOX) hit = Physics2D.BoxCast(transform.position + ((offSet * (isRootMinus ? -1 : 1)) * rootMag), (vBoxSize * rootMag), 0, Vector2.zero, 0, mask);
        else if (cType == CollisionType.CIRCLE) hit = Physics2D.CircleCast(transform.position + ((offSet * (isRootMinus ? -1 : 1)) * rootMag), (fCircleRadius * rootMag), Vector2.zero, 0, mask);
        else hit = Physics2D.CapsuleCast(transform.position + ((offSet * (isRootMinus ? -1 : 1)) * rootMag), vBoxSize, capsuleDirection, 0, Vector2.zero, 0, mask);

        if (hit && !isUsing)
        {
            isUsing = true;
            hit.collider.GetComponent<AnimController>().DoorInMethod(DoorInObj.transform.position, DoorOutObj.transform.position, this);
        }
    }
}
