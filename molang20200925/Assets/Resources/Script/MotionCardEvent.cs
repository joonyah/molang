﻿using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotionCardEvent : MonoBehaviour
{

    public string motionName;
    public string motionSkin;

    public Camera MotionCamera;
    public GameObject MotionPopupObj;
    public SkeletonAnimation sAnim;
    SkeletonDataAsset sAsset;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void MotionStart()
    {
        if (sAsset == null)
            sAsset = sAnim.SkeletonDataAsset;
        sAnim.ClearState();
        sAnim.skeletonDataAsset = sAsset;
        sAnim.initialSkinName = motionSkin;
        sAnim.Initialize(true);

        if (motionName.Contains("swim"))
            MotionCamera.transform.localPosition = new Vector3(-56000, 4716);
        else
            MotionCamera.transform.localPosition = new Vector3(-36160, 4716);

        MotionPopupObj.SetActive(true);

        if (motionName.Equals("tool_pickup"))
        {
            motionName = "action_idle";
            if (sAnim.transform.GetChild(0))
                sAnim.transform.GetChild(0).gameObject.SetActive(true);
        }
        else
        {
            if (sAnim.transform.GetChild(0))
                sAnim.transform.GetChild(0).gameObject.SetActive(false);
        }

        sAnim.AnimationName = motionName;
    }
}
