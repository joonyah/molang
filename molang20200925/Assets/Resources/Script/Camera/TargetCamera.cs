﻿using UnityEngine;

public class TargetCamera : MonoBehaviour
{
    public static TargetCamera instance;



    public float smoothTime = 0.3F;
    public float zOffSet = -5f;

    public Transform target;

    private Vector2 velocity = Vector2.zero;
    private Vector2 weaponDir = Vector2.zero;
    public float CameraMoveAmount = 2.0f;

    private void Awake()
    {
        if (instance == null) instance = this;
    }

    private void Update()
    {
        if (target == null) return;
        Vector2 targetPosition = target.TransformPoint(new Vector2());
        weaponDir = Vector2.zero;

        targetPosition.y += 0.5f;
        targetPosition += (weaponDir * CameraMoveAmount);
        transform.position = Vector2.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
        transform.position = new Vector3(transform.position.x, transform.position.y, zOffSet);
    }
}