﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomFog : MonoBehaviour
{
    //
    private Camera UICamera;
    private float fCamSize;

    //
    private bool isZoomOut = false;

    //
    public float fZoomOffset = 50f;
    public float fFogSpeed = 2.5f;

    //안개 움직임 효과
    public TweenPosition tweenLeft;
    public TweenPosition tweenRight;

    void Start()
    {
        UICamera = this.transform.parent.GetComponent<Camera>();

        tweenLeft.duration = fFogSpeed;
        tweenRight.duration = fFogSpeed;
    }

    void LateUpdate()
    {
        if(UICamera == null)
        {
            Debug.LogError("ZoomFog Not Found Camera");
            return;
        }

        fCamSize = UICamera.orthographicSize;
        if(fCamSize > fZoomOffset && !isZoomOut)
        {
            isZoomOut = true;

            tweenLeft.enabled = true;
            //tweenLeft.ResetToBeginning();
            tweenLeft.PlayForward();

            tweenRight.enabled = true;
            //tweenRight.ResetToBeginning();
            tweenRight.PlayForward();
        }
        else if (fCamSize <= fZoomOffset && isZoomOut)
        {
            isZoomOut = false;

            tweenLeft.enabled = true;
            tweenLeft.PlayReverse();

            tweenRight.enabled = true;
            tweenRight.PlayReverse();
        }
    }
}
