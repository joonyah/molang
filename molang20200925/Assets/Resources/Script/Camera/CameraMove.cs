﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CameraMove : MonoBehaviour
{
    public Camera UICamera;
    Vector3 hit_position = Vector3.zero;
    Vector3 current_position = Vector3.zero;
    Vector3 camera_position = Vector3.zero;

    public bool isCameraMove = true;

    // Start is called before the first frame update
    void Start()
    {

    }

    void Update()
    {
        // Step Position 갱신용
        //for (int i = 0; i < uiController.BuildStepObjs.Length; i++)
        //{
        //    Vector3 screenPoint = Camera.main.WorldToScreenPoint(uiController.StepPositions[i].position);
        //    Vector3 uiPoint = UICamera.ScreenToWorldPoint(screenPoint);
        //    uiPoint.z = 0;
        //    uiController.BuildStepObjs[i].transform.position = uiPoint;
        //}
        if (!isCameraMove) return;
        if (Input.GetMouseButtonDown(0))
        {
            hit_position = Input.mousePosition;
            camera_position = UICamera.transform.position;
        }
        if (Input.GetMouseButton(0))
        {
            current_position = Input.mousePosition;
            LeftMouseDrag();
        }

    }

    void LeftMouseDrag()
    {
        if (!isCameraMove) return;
        current_position.z = hit_position.z = camera_position.y;
        Vector3 direction = UICamera.ScreenToWorldPoint(current_position) - UICamera.ScreenToWorldPoint(hit_position);
        direction = direction * -1;
        Vector3 position = camera_position + direction;
        UICamera.transform.position = position;
    }
}
