﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ScreenShotTestScript : MonoBehaviour
{
    public float minX;
    public float maxX;
    public float minY;
    public float maxY;

    public float moveX = 49079.44f;
    public float moveY = -25600;
    
    public string path = "C:/TestScreenShot/";

    public int resWidth = 1915;
    public int resHeight = 993;

    public bool isTest;
    public Camera camera;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(isTest)
        {
            isTest = false;
            StartCoroutine(Test());
        }
    }

    IEnumerator Test()
    {
        int count = 0;
        camera.transform.localPosition = new Vector3(minX, maxY);
        while (true)
        {
            ClickScreenShot();
            if (camera.transform.localPosition.x < maxX)
            {
                camera.transform.localPosition = new Vector3(camera.transform.localPosition.x + moveX, camera.transform.localPosition.y);
            }
            else
            {
                if (camera.transform.localPosition.y < minY) break;
                camera.transform.localPosition = new Vector3(minX, camera.transform.localPosition.y + moveY);
            }
            count++;
            Debug.Log(count);
            yield return new WaitForSeconds(1.0f);
        }
    }

    public void ClickScreenShot()
    {
        DirectoryInfo dir = new DirectoryInfo(path);
        if (!dir.Exists)
        {
            Directory.CreateDirectory(path);
        }
        string name;
        name = path + System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + ".png";
        RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);
        camera.targetTexture = rt;
        Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
        Rect rec = new Rect(0, 0, screenShot.width, screenShot.height);
        camera.Render();
        RenderTexture.active = rt;
        screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
        screenShot.Apply();

        byte[] bytes = screenShot.EncodeToPNG();
        File.WriteAllBytes(name, bytes);
    }
}
