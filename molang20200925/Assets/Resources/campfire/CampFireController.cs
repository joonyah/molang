﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampFireController : MonoBehaviour
{
    [SerializeField] private Vector3 offSet;
    [SerializeField] private Vector3 vBoxSize;
    [SerializeField] private float fCircleRadius;
    [SerializeField] private CollisionType cType;
    [SerializeField] private CapsuleDirection2D capsuleDirection;
    [SerializeField] private LayerMask mask;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        if (cType == CollisionType.BOX) Gizmos.DrawWireCube(transform.position + offSet, vBoxSize);
        else if (cType == CollisionType.CIRCLE) Gizmos.DrawWireSphere(transform.position + offSet, fCircleRadius);
    }

    private void Update()
    {
        RaycastHit2D hit;

        if (cType == CollisionType.BOX) hit = Physics2D.BoxCast(transform.position + offSet, vBoxSize, 0, Vector2.zero, 0, mask);
        else if (cType == CollisionType.CIRCLE) hit = Physics2D.CircleCast(transform.position + offSet, fCircleRadius, Vector2.zero, 0, mask);
        else hit = Physics2D.CapsuleCast(transform.position + offSet, vBoxSize, capsuleDirection, 0, Vector2.zero, 0, mask);

        if (hit)
        {
            if(!hit.collider.GetComponent<AnimController>().isCampfire)
            {
                hit.collider.GetComponent<AnimController>().isCampfire = true;
                hit.collider.GetComponent<CharacterAI>().isMoveStop = true;
                hit.collider.GetComponent<AnimController>().isTool_pickup = true;

                Vector3 hitPos = hit.collider.transform.position;
                Vector3 dir = transform.position - hitPos;
                dir.Normalize();
                if (dir.x > 0) hit.collider.transform.localScale = new Vector3(Mathf.Abs(hit.collider.transform.localScale.x) * -1, hit.collider.transform.localScale.y, hit.collider.transform.localScale.z);
                else hit.collider.transform.localScale = new Vector3(Mathf.Abs(hit.collider.transform.localScale.x), hit.collider.transform.localScale.y, hit.collider.transform.localScale.z);
            }
        }
    }
}
